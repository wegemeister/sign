# 
function(PrintVariables)
    message(STATUS "  VARS:")
    set(LIST
        "CMAKE_SOURCE_DIR" "CMAKE_BINARY_DIR"
        "PROJECT_NAME" "PROJECT_VERSION"
        "PROJECT_SOURCE_DIR" "PROJECT_BINARY_DIR"
        "CMAKE_CURRENT_SOURCE_DIR" "CMAKE_CURRENT_BINARY_DIR"
        "CMAKE_CURRENT_LIST_FILE" "CMAKE_CURRENT_LIST_DIR"
        ${ARGV}
    )
    foreach(VAR ${LIST})
        message(STATUS "    ${VAR} : ${${VAR}}")
    endforeach()
endfunction()
