cmake_minimum_required(VERSION 3.9)

file(TO_CMAKE_PATH "${PROJECT_BINARY_DIR}/CMakeLists.txt" LOC_PATH)
if(EXISTS "${LOC_PATH}")
  message(
    FATAL_ERROR
      "You cannot build in a source directory (or any directory with a CMakeLists.txt file). Please make a build subdirectory. Feel free to remove CMakeCache.txt and CMakeFiles."
    )
endif()

project(
  sign
  LANGUAGES C CXX
  VERSION 0.1.0 DESCRIPTION
          "Genarator für Darstellung von Wegweisertafeln Wandern und Radfahren."
  )

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
# set(CMAKE_CXX_EXTENSIONS OFF)

# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")

# set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage")

# set(CMAKE_EXE_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS} -fprofile-arcs -ftest-
# coverage")

# global compile options
if(NOT MSVC)
  # add_compile_options("-fsanitize=address")
  add_compile_options(-Wall
                      -Wextra
                      -Wshadow
                      -Wconversion
                      -Wnon-virtual-dtor
                      -pedantic)
endif()

# external Dependencies

# boost
set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
find_package(Boost COMPONENTS program_options REQUIRED)

# libxml++ , glibmm
find_package(PkgConfig REQUIRED)
pkg_search_module(GLIBMM REQUIRED glibmm-2.4)
pkg_search_module(LIBXMLPP REQUIRED libxml++-3.0)
find_package(GTest)

# Tools: clang-tidy
find_program(CLANG_TIDY NAMES "clang-tidy")

set(
  CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY}"
  "-checks=~*,bugprone~*,cppcoreguidelines~*,-clang-analyzer-deadcode.DeadStores"
  )

# configure a header file to pass some of the CMake settings to the source code
configure_file("${PROJECT_SOURCE_DIR}/src/cmakeConfig.h.in"
               "${PROJECT_BINARY_DIR}/cmakeConfig.h")

include(CTest)

# bin directory
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

add_library(${PROJECT_NAME}
            src/ColorManager.cpp
            src/Font.cpp
            src/FontManager.cpp
            src/Glyph.cpp
            src/JSONParser.cpp
            src/Services.cpp
            src/Sign.cpp
            src/SignData.cpp
            src/SignDataNode.cpp
            src/Svg.cpp
            src/SymbolLib.cpp
            src/SymbolManager.cpp
            src/XMLParser.cpp
            src/Signs/De_Thuer_Wanderwegweiser.cpp
            src/SymbolLibs/de_thuer_Wanderwegweisung_Wegemarken.cpp
            src/SymbolLibs/de_thuer_Wanderwegweisung_Zielsymbole.cpp)

target_include_directories(
  ${PROJECT_NAME}
  PUBLIC "${CMAKE_SOURCE_DIR}/include"
         "${PROJECT_BINARY_DIR}"
         "${CMAKE_SOURCE_DIR}/extern/rapidjson-master/include"
         ${Boost_INCLUDE_DIRS}
         ${GLIBMM_INCLUDE_DIRS}
         ${LIBXMLPP_INCLUDE_DIRS}
         "/usr/include"
  PRIVATE)

target_link_libraries(${PROJECT_NAME}
                      ${Boost_PROGRAM_OPTIONS_LIBRARY}
                      ${GLIBMM_LIBRARIES}
                      ${LIBXMLPP_LIBRARIES})

add_executable(${PROJECT_NAME}master src/main.cpp)

set_target_properties(${PROJECT_NAME}master
                      PROPERTIES LIBRARY_OUTPUT_DIRECTORY
                                 ${CMAKE_SOURCE_DIR}/bin
                                 RUNTIME_OUTPUT_DIRECTORY
                                 ${CMAKE_SOURCE_DIR}/bin)

target_link_libraries(${PROJECT_NAME}master ${PROJECT_NAME})

add_subdirectory(tests)
