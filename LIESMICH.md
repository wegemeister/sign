# Sign

Diese Konsole-Applikation nimmt ein JSON oder XML Objekt der Schildermeister (<https://gitlab.com/wegemeister/schildermeister>) Schema-Spezifikation und verweandelt es in eine graphische Darstellung.

## Verfügbare Wegweisungs Standards

* europe/de/de-th/Wanderwegweisertafel: Wanderwegweisertafel nach dem "Praxisleitfaden - Touristische Wanderwegkonzeption." : <http://apps.thueringen.de/de/publikationen/pic/pubdownload1744.pdf>

## Benutzung

sign [options]

options :

   --help [-h] Aufruf-Hilfe

   --infile [-i] InputfileName oder '-' für stdin, Standardwert ist stdin ('-')

   --inputformat [-d] Inputformat [json, xml], Standardwert ist die Eingabedatei-Extension oder 'xml'

   --outfile [-o] OutputfileName oder '-' für stdout, Standardwert ist stdout '-'

   --outputformat [-f] Outputformat [svg | png | jpg | bmp], Standardwert ist die Ausgabedatei-Extension or 'svg'

   --bitmapmaxwidth [-W] max Breite in pixel für Bitmapformate

   --bitmapmaxwidth [-H] max Höhe in pixel für Bitmapformats

   --version [-v] : Shows some Version information

## Beispiel

![wegweiser](./Examples/Beispiel01.svg)

from:

```json
{
   "self": "https://wegemeister.de/Schemata/europe/de/de-th/Wanderwegweisertafel.json",
   "version": "1",
   "Schildernummer": "Schild-Nr.: 16066_042_WW_0032_A2",
   "Wegemarken": ["blauer Balken"],
   "Ausrichtung": 12,
   "Nahziel": {
      "Info": "Gebiet $Wegename$ (40ppt)",
      "Name": "Nahziel (80ppt)",
      "Entfernung": 2000,
      "Zielsymbole": ["Aussicht"]
   },
   "Fernziel": {
      "Info": "(Ergänzung zumFernziel) (40ppt)",
      "Name": "Fernziel (80ppt)",
      "Entfernung": 10300,
      "Zielsymbole": ["Bahnhof", "Übernachtung"]
   },
   "Ausführung": {
      "Richtung": "links",
      "Montageposition": "mitte",
      "Löcher": true,
      "Lochabstand": 350
   }
}
```

Weitere Beispiele im Ordner Examples.

## Überblick

Die Konsoleapp ist in C++ geschrieben nutzt C++17 Features und libxml sowie den rapidjason sax Parser.

### Voraussetzungen

* libxml++3
* librsvg
* boost
* glibmm++

### ...nutzt

* rapidJSON

### Konzepte

Das Grundkonzept ist die Generierung eine SVG Objekts aus JSON/XML Beschreibungen der Schildermeister (<https://gitlab.com/wegemeister/schildermeister>) Schema-Spezifikation.

Eine Basis-Klasse sign mit einer virtuellen Funktion createSign initiiert den svg Gestaltungsprozess. Von dieser Basisklasse leiten alle Schilderklassen ab und überschreiben die createsign Funktion. Ergänzend gibt es Unterstützungsklassen für das Handhaben und Zwischenspeichern für Fonts, Symbole und Farben.

In der Spezifikation kann ein Status aus ["Planung", "Bestand", "Abbau"] angegeben werden. Der Standardwert ist "Planung". Dieser Status bewirkt:

* "Planung": farbige Darstellung - Standard
* "Bestand": Graustufen Darstellung
* "Abbau": Graustufen Darstellung, rot ausgekreuzt

## Lizenz

Diese Software ist verfügbar unter der GPLV3, die Lizenzen der externen Bibliotheken unter external werden respektiert.
Für anderen Lizensierungsmodelle kontaktieren Sie bitte den Autor.

## Erweiterung, Ergänzungen

Die Software ist ein Tool welches für die interne Nutzung programmiert wurde. Erweiterungen und Ergänzungen richten sich nach unserem Bedarf. Anregungen, Beteiligungen und Ergänzungen sind erwünscht, umfangreichere Anpassungen möglich.

### geplante Erweiterungen

* shared Library zur Einbindung in andere Software
* Windows Version 64bit
* Radwegweisung Thüringen

## Autor

(c) 2019 Ervin Peters (<coder@wegemeister.de>, <https://wegemeister.de>)  
