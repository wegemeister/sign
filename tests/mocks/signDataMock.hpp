#ifndef SIGNDATAMOCK_HPP
#define SIGNDATAMOCK_HPP

#include <glibmm.h>

namespace signmaster
{

class SignDataMock
{
  public:
    SignDataMock();
    SignDataMock(const Glib::ustring &name, SignDataMock *parent);
    ~SignDataMock();

    void addAttribute(Glib::ustring name, Glib::ustring value);
    bool hasAttributes();
    Glib::ustring attribute(Glib::ustring name);

    // Handling of children
    void addChild(Glib::ustring name, SignDataMock *child);
};

} // namespace signmaster

#endif