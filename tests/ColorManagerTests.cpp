
// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <gtest/gtest.h>

#include <sign/ColorManager.hpp>
#include "tests.hpp"

using namespace signmaster;

struct RelatedColor
{
    std::string name;
    Glib::ustring colorName;
    std::string color;
    std::string grey;
};

class ColorManagerTest : public ::testing::TestWithParam<RelatedColor>
{
    // You can implement all the usual fixture class members here.
    // To access the test parameter, call GetParam() from class
    // TestWithParam<T>.
};

std::string colorName(testing::TestParamInfo<RelatedColor> p) { return p.param.name; }

TEST_P(ColorManagerTest, hexRGB2Grey)
{
    RelatedColor row = GetParam();
    ColorManager cm;
    ASSERT_EQ(cm.hexRGB2Grey(row.color), row.grey);
}

TEST_P(ColorManagerTest, getColor)
{
    RelatedColor row = GetParam();
    ColorManager cm;
    ASSERT_EQ(cm.getColor(row.name), row.color);
}

INSTANTIATE_TEST_CASE_P(FewColorManagerColorTest, ColorManagerTest, ::testing::Values(RelatedColor{"none", "none", "none", "none"}, RelatedColor{"schwarz", "schwarz", "#000000", "#191919"}, RelatedColor{"weiss", "weiss", "#ffffff", "#e5e5e5"}, RelatedColor{"rot", "rot", "#ff0000", "#555555"}, RelatedColor{"gruen", "gruen", "#00ff00", "#909090"}, RelatedColor{"blau", "blau", "#0000ff", "#303030"}), colorName);
