
// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <gtest/gtest.h>

#include <sign/Svg.hpp>
#include "tests.hpp"

using namespace signmaster;

TEST(ESvgTests, default_constructor_creates)
{
    ASSERT_NO_THROW(Svg svg);
}

TEST(ESvgTests, check_pointer_not_null)
{
    Svg svg;
    ASSERT_NE(svg.root(), nullptr);
    ASSERT_NE(svg.defs(), nullptr);
}

TEST(ESvgTests, default_svg_frame_output)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/minimal.svg"));
    Svg svg;
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}

TEST(ESvgTests, check_svg_output_root_viewBox)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/viewbox.svg"));
    Svg svg;
    svg.setViewBox(nullptr, 1, 2, 3, 4);
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}

TEST(ESvgTests, check_svg_output_appendLine)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/line.svg"));
    Svg svg;
    svg.appendLine(svg.root(), "line43", 1, 2, 3, 4, "#000000", 1, "none");
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}

TEST(ESvgTests, check_svg_output_appendCircle)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/circle.svg"));
    Svg svg;
    svg.appendCircle(nullptr, "circle42", 10, 20, 5, "#000000", 1, "none");
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}

TEST(ESvgTests, check_svg_output_appendPath)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/path.svg"));
    Svg svg;
    svg.appendPath(nullptr, "Path01", "M 0,0 l 10,10 z", "#000000", 1, "none");
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}

TEST(ESvgTests, check_svg_output_appendRectangle)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/rect.svg"));
    Svg svg;
    svg.appendRectangle(svg.root(), "rect45", 1, 2, 3, 4, "#000000", 1, "none");
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}

TEST(ESvgTests, check_svg_output_appendGroup)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/group.svg"));
    Svg svg;
    svg.appendGroup(nullptr, "group98");
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}

TEST(ESvgTests, check_svg_output_appendSymbol)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/symbol.svg"));
    Svg svg;
    svg.symbolNamePath("europe/de/de-th/Wanderwegweisung/");
    svg.appendSymbol(nullptr, "info", 1, 2, 100, 200, "Info", "Zielsymbole", "Planung");
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}

TEST(ESvgTests, check_svg_output_appendText)
{
    std::string should = withoutWhiteSpaces(dataFileAsString("svg/text.svg"));
    Svg svg;
    double width = 200;
    svg.appendText(nullptr, "t1", 10, 11, width, 'l', "T", "Arial", 20, "#000000");
    std::stringstream is;
    svg.writeSvgToStream(is);
    ASSERT_EQ(withoutWhiteSpaces(is.str()), should);
}
