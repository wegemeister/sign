
// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of De_Thuer_Wanderwegweiser_Content:: <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with De_Thuer_Wanderwegweiser_Content:: If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von De_Thuer_Wanderwegweiser_Content:: <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <gtest/gtest.h>

#include <signs/De_Thuer_Wanderwegweiser.hpp>
#include <sign/SignDataNode.hpp>
#include "../tests.hpp"

using namespace signmaster;

TEST(De_Thuer_Wanderwegweiser_Content_Tests, format_km)
{
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(49), "0,0 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(50), "0,1 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(100), "0,1 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(149), "0,1 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(150), "0,2 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(9900), "9,9 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(9950), "10 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(25000), "25 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(24999), "25 km");
    ASSERT_EQ(De_Thuer_Wanderwegweiser_Content::formatKm(125499), "125 km");
}
