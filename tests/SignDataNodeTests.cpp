

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <gtest/gtest.h>

#include <sign/SignDataNode.hpp>

using namespace signmaster;

TEST(SignDataNodeTests, default_constructor_root_state_checks)
{
    SignDataNode sdn;
    ASSERT_EQ(sdn.type(), nodeEMPTY);
    ASSERT_EQ(sdn.parent(), nullptr);
    ASSERT_EQ(sdn.name(), "root");
}

TEST(SignDataNodeTests, constructor_child_state_checks)
{
    SignDataNode parent;
    SignDataNode *child = new SignDataNode(&parent);
    ASSERT_EQ(child->type(), nodeEMPTY);
    ASSERT_EQ(child->parent(), &parent);
    ASSERT_EQ(child->name(), "");
    ASSERT_TRUE(parent.hasChildren());
    ASSERT_EQ(parent.countChildren(), 1);
}

TEST(SignDataNodeTests, constructor_named_child_state_checks)
{
    SignDataNode parent;
    std::string childName = "cn_örv";
    SignDataNode *child = new SignDataNode(childName, &parent);
    ASSERT_EQ(child->type(), nodeEMPTY);
    ASSERT_EQ(child->parent(), &parent);
    ASSERT_EQ(child->name(), childName);
}

TEST(SignDataNodeTests, constructor_named_child_with_value_state_checks)
{
    SignDataNode parent;
    std::string childName = "cn_örv";
    std::string value = "valueµ";
    SignDataNode *child = new SignDataNode(childName, value, &parent);
    ASSERT_EQ(child->type(), nodeSTRING);
    ASSERT_EQ(child->parent(), &parent);
    ASSERT_EQ(child->asString(), value);
    ASSERT_EQ(child->name(), childName);
}

TEST(SignDataNodeTests, set_int64_state_checks)
{
    SignDataNode node;
    int64_t value = 420000000000043;
    ASSERT_NO_THROW(node.Int64(value));
    ASSERT_EQ(node.type(), nodeINT64);
    ASSERT_EQ(node.asInt64(), value);
    ASSERT_EQ(std::stol(node.asString()), value);
}

TEST(SignDataNodeTests, set_double_state_checks)
{
    SignDataNode node;
    double value = 4200.00000000043;
    ASSERT_NO_THROW(node.Double(value));
    ASSERT_EQ(node.type(), nodeDOUBLE);
    ASSERT_EQ(node.asDouble(), value);
    ASSERT_EQ(std::stod(node.asString()), value);
}

TEST(SignDataNodeTests, set_bool_state_checks)
{
    SignDataNode node;
    bool value = true;
    ASSERT_NO_THROW(node.Bool(value));
    ASSERT_EQ(node.type(), nodeBOOL);
    ASSERT_EQ(node.asBool(), value);
    ASSERT_EQ(node.asBoolString(), (value ? "true" : "false"));
}

//TEST(SignDataNodeTests,
