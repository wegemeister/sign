

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <gtest/gtest.h>

#include <sign/JSONParser.hpp>

using namespace signmaster;

TEST(JSONParserTests, parse_empty_json_throws)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    Glib::ustring json("");
    ASSERT_THROW(jsonParser.parse_memory(json), SignMasterException);
}

TEST(JSONParserTests, parse_minimal_json_not_throws)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    Glib::ustring json("{}");
    ASSERT_NO_THROW(jsonParser.parse_memory(json));
}

TEST(JSONParserTests, parse_json_with_ascii_keyvalue_not_throws)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    Glib::ustring json("{\"oerv\":\"phi1\"}");
    ASSERT_NO_THROW(jsonParser.parse_memory(json));
    ASSERT_EQ(sd.child(0)->name(), "oerv");
    ASSERT_EQ(sd.child(0)->asString(), "phi1");
}

TEST(JSONParserTests, parse_json_with_ascii_key_unicode_value_not_throws)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    Glib::ustring json("{\"oerv\":\"ø1\"}");
    ASSERT_NO_THROW(jsonParser.parse_memory(json));
}

TEST(JSONParserTests, parse_json_with_unicode_keyvalue_not_throws)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    Glib::ustring json("{\"örv\":\"ø1\"}");
    ASSERT_NO_THROW(jsonParser.parse_memory(json));
}

TEST(JSONParserTests, parse_json_with_missing_key_throws)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    Glib::ustring json("{: 8}");
    ASSERT_THROW(jsonParser.parse_memory(json), SignMasterException);
}

TEST(JSONParserTests, parse_json_with_missing_value_throws)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    Glib::ustring json("{ \"key\" : }");
    ASSERT_THROW(jsonParser.parse_memory(json), SignMasterException);
}

TEST(JSONParserTests, parse_json_complexity_check_with_grandchildren)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    std::string json("{\"ä\":{ \"colör\" : \"øø\"}, \"o\":{ \"a\" :\"2\"} }");
    ASSERT_NO_THROW(jsonParser.parse_memory(json));
    ASSERT_TRUE(sd.child(0)->hasChildren());
    SignDataNode *gs = sd.child(1);
    ASSERT_EQ(gs->name(), "o");
    ASSERT_TRUE(gs->hasChildren());
    ASSERT_EQ(gs->child("a")->asString(), "2");
}

TEST(JSONParserTests, parse_types_json_not_throws)
{
    SignDataNode sd;
    JSONParser jsonParser(sd);
    Glib::ustring json("{ \"a\" : null, \"b\" : true, \"c\" : false, \"d\" : 42, \"e\" : 4200000000000000042, \"f\" : -42, \"g\" : -4200000000000000042, \"h\" :  42.42, \"i\" : 42424242242424244242424242442424424224244222424222222242424242442, \"j\" : \"MY42String\", \"k\" : [1,2,3], \"l\" : [\"werner\", 42,42, true, [true, 5], {\"name\":\"lari\", \"age\": 20, \"sex\": \"f\"}]}");
    ASSERT_NO_THROW(jsonParser.parse_memory(json));
}
