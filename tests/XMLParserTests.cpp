

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <gtest/gtest.h>

#include <sign/XMLParser.hpp>

using namespace signmaster;

TEST(XMLParserTests, parse_empty_xml_throws)
{
    SignDataNode sd;
    XMLParser xmlParser(&sd);
    Glib::ustring xml("");
    ASSERT_THROW(xmlParser.parse_memory(xml), std::exception);
}

TEST(XMLParserTests, parse_minimal_xml_not_throws)
{
    SignDataNode sd;
    XMLParser xmlParser(&sd);
    Glib::ustring xml("<?xml version='1.0'?><_/>");
    ASSERT_NO_THROW(xmlParser.parse_memory(xml));
}

TEST(XMLParserTests, parse_xml_with_unicode_names_not_throws)
{
    SignDataNode sd;
    XMLParser xmlParser(&sd);
    Glib::ustring xml("<?xml version='1.0'?><ä></ä>");
    ASSERT_NO_THROW(xmlParser.parse_memory(xml));
    ASSERT_EQ((sd.child(0))->name(), "ä");
}

TEST(XMLParserTests, parse_xml_with_unicode_attribut_not_throws)
{
    SignDataNode sd;
    XMLParser xmlParser(&sd);
    Glib::ustring xml("<?xml version='1.0'?><ä colör=\"øø\"></ä>");
    ASSERT_NO_THROW(xmlParser.parse_memory(xml));
    ASSERT_EQ((sd.child(0))->child("colör")->asString(), "øø");
}

TEST(XMLParserTests, parse_xml_with_grandchildren)
{
    SignDataNode sd;
    XMLParser xmlParser(&sd);
    Glib::ustring xml("<?xml version='1.0'?><ä colör=\"øø\"><o a=\"2\" /></ä>");
    ASSERT_NO_THROW(xmlParser.parse_memory(xml));
    ASSERT_TRUE(sd.child(0)->hasChildren());
    ASSERT_EQ((sd.child(0))->child("colör")->asString(), "øø");
    SignDataNode *gs = sd.child(0)->child("o")->child(0);
    ASSERT_EQ(gs->name(), "a");
}

TEST(XMLParserTests, parse_invalid_start_end_combination)
{
    SignDataNode sd;
    XMLParser xmlParser(&sd);
    Glib::ustring xml("<?xml version='1.0'?><svg><aa /></scg>");
    ASSERT_THROW(xmlParser.parse_memory(xml), xmlpp::parse_error);
}

TEST(XMLParserTests, parse_xml_with_one_text_Elements)
{
    SignDataNode sd;
    XMLParser xmlParser(&sd);
    Glib::ustring xml("<?xml version='1.0'?><svg>some text content2</svg>");
    ASSERT_NO_THROW(xmlParser.parse_memory(xml));
    SignDataNode *child = sd.child(0);
    ASSERT_TRUE(child->hasChildren());
    ASSERT_EQ(child->name(), "svg");
    ASSERT_EQ(child->child(0)->asString(), "some text content2");
}
