#include "tests.hpp"

#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <cctype>
#include <algorithm>

namespace signmaster
{

std::string dataFileAsString(std::string fileName)
{
    std::string mypath = srcPath + "/tests/data/" + fileName;
    // std::cout << mypath << " : " << std::filesystem::current_path() << std::endl;
    std::ifstream ifs(mypath);
    std::string content((std::istreambuf_iterator<char>(ifs)),
                        (std::istreambuf_iterator<char>()));

    return content;
}

std::string withoutWhiteSpaces(std::string data)
{
    data.erase(std::remove_if(data.begin(), data.end(), ::iswspace), data.end());
    // std::cout << data << std::endl;
    return data;
}

} // namespace signmaster
