
// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <sign/ColorManager.hpp>

#include <iomanip>

namespace signmaster
{

std::string ColorManager::hexRGB2Grey(const std::string &color)
{
    int red;
    int green;
    int blue;
    if (color == "none")
        return color;
    if (color.length() == 4)
    {
        red = std::stoi(color.substr(1, 1), nullptr, 16);
        green = std::stoi(color.substr(2, 1), nullptr, 16);
        blue = std::stoi(color.substr(3, 1), nullptr, 16);
        red |= red << 8;
        green |= green << 8;
        blue |= blue << 8;
    }
    else if (color.length() == 7)
    {
        red = std::stoi(color.substr(1, 2), nullptr, 16);
        green = std::stoi(color.substr(3, 2), nullptr, 16);
        blue = std::stoi(color.substr(5, 2), nullptr, 16);
    }
    else
    {
        throw SignMasterException("ColorManager: invalid color");
    }
    int grau = ((0.299 * red + 0.587 * green + 0.114 * blue) * 0.8 + 25);
    std::stringstream ss;
    ss << "#" << std::hex << std::setw(2) << grau << grau << grau;
    std::string ret(ss.str());
    return (ret);
}

std::string ColorManager::getColor(const std::string &color, bool greyify)
{
    if (color == "none")
        return (color);
    if (color.substr(0, 1) == "#")
    {
        return (greyify ? hexRGB2Grey(color) : color);
    }
    std::string hexColor = colors.at(color);
    return (greyify ? hexRGB2Grey(hexColor) : hexColor);
}

ColorManager::ColorManager() : colors({
                                   // Deutsche Farbnamen ? https: //bfw.ac.at/020/farbtabelle.html
                                   {"weiß", "#ffffff"},
                                   {"weiss", "#ffffff"},
                                   {"rot", "#ff0000"},
                                   {"gelb", "#ffff00"},
                                   {"grün", "#00ff00"},
                                   {"gruen", "#00ff00"},
                                   {"blau", "#0000ff"},
                                   {"schwarz", "#000000"},
                                   {"grau", "#8f8f8f"},
                                   {"grau1", "#5f5f5f"},

                                   //web
                                   {"aliceblue", "#F0F8FF"},
                                   {"antiquewhite", "#FAEBD7"},
                                   {"aqua", "#00FFFF"},
                                   {"aquamarine", "#7FFFD4"},
                                   {"azure", "#F0FFFF"},
                                   {"beige", "#F5F5DC"},
                                   {"black", "#000000"},
                                   {"blue", "#0000FF"},
                                   {"blueviolet", "#8A2BE2"},
                                   {"brown", "#A52A2A"},
                                   {"burlywood", "#DEB887"},
                                   {"cadetblue", "#5F9EA0"},
                                   {"chartreuse", "#7FFF00"},
                                   {"chocolate", "#D2691E"},
                                   {"coral", "#FF7F50"},
                                   {"cornflowerblue", "#6495ED"},
                                   {"cornsilk", "#FFF8DC"},
                                   {"crimson", "#DC143C"},
                                   {"darkblue", "#00008B"},
                                   {"darkcyan", "#008B8B"},
                                   {"darkgoldenrod", "#B8860B"},
                                   {"darkgray", "#A9A9A9"},
                                   {"darkgreen", "#006400"},
                                   {"darkkhaki", "#BDB76B"},
                                   {"darkmagenta", "#8B008B"},
                                   {"darkolivegreen", "#556B2F"},
                                   {"darkorange", "#FF8C00"},
                                   {"darkorchid", "#9932CC"},
                                   {"darkred", "#8B0000"},
                                   {"darksalmon", "#E9967A"},
                                   {"darkseagreen", "#8FBC8F"},
                                   {"darkslateblue", "#483D8B"},
                                   {"darkslategray", "#2F4F4F"},
                                   {"darkturquoise", "#00CED1"},
                                   {"darkviolet", "#9400D3"},
                                   {"deeppink", "#FF1493"},
                                   {"deepskyblue", "#00BFFF"},
                                   {"dimgray", "#696969"},
                                   {"dodgerblue", "#1E90FF"},
                                   {"firebrick", "#B22222"},
                                   {"floralwhite", "#FFFAF0"},
                                   {"forestgreen", "#228B22"},
                                   {"fuchsia", "#ff00ff"},
                                   {"gainsboro", "#DCDCDC"},
                                   {"ghostwhite", "#F8F8FF"},
                                   {"gold", "#FFD700"},
                                   {"goldenrod", "#DAA520"},
                                   {"gray", "#808080"},
                                   {"green", "#00FF00"},
                                   {"greenyellow", "#ADFF2F"},
                                   {"honeydew", "#F0FFF0"},
                                   {"hotpink", "#FF69B4"},
                                   {"indianred", "#CD5C5C"},
                                   {"indigo", "#4B0082"},
                                   {"ivory", "#FFFFF0"},
                                   {"khaki", "#F0E68C"},
                                   {"lavender", "#E6E6FA"},
                                   {"lavenderblush", "#FFF0F5"},
                                   {"lawngreen", "#7CFC00"},
                                   {"lemonchiffon", "#FFFACD"},
                                   {"lightblue", "#ADD8E6"},
                                   {"lightcoral", "#F08080"},
                                   {"lightcyan", "#E0FFFF"},
                                   {"lightgoldenrodyellow", "#FAFAD2"},
                                   {"lightgreen", "#90EE90"},
                                   {"lightgrey", "#D3D3D3"},
                                   {"lightpink", "#FFB6C1"},
                                   {"lightsalmon", "#FFA07A"},
                                   {"lightseagreen", "#20B2AA"},
                                   {"lightskyblue", "#87CEFA"},
                                   {"lightslategray", "#778899"},
                                   {"lightsteelblue", "#B0C4DE"},
                                   {"lightyellow", "#FFFFE0"},
                                   {"lime", "#00FF00"},
                                   {"limegreen", "#32CD32"},
                                   {"linen", "#FAF0E6"},
                                   {"maroon", "#800000"},
                                   {"mediumaquamarine", "#66CDAA"},
                                   {"mediumblue", "#0000CD"},
                                   {"mediumorchid", "#BA55D3"},
                                   {"mediumpurple", "#9370D "},
                                   {"mediumseagreen", "#3CB371"},
                                   {"mediumslateblue", "#7B68EE"},
                                   {"mediumspringgreen", "#00FA9A"},
                                   {"mediumturquoise", "#48D1CC"},
                                   {"mediumvioletred", "#C71585"},
                                   {"midnightblue", "#191970"},
                                   {"mintcream", "#F5FFFA"},
                                   {"mistyrose", "#FFE4E1"},
                                   {"moccasin", "#FFE4B5"},
                                   {"navy", "#000080"},
                                   {"navajowhite", "#FFDEAD"},
                                   {"oldlace", "#FDF5E6"},
                                   {"olive", "#808000"},
                                   {"olivedrab", "#6B8E23"},
                                   {"orange", "#FFA500"},
                                   {"orangered", "#FF4500"},
                                   {"orchid", "#DA70D6"},
                                   {"palegoldenrod", "#EEE8AA"},
                                   {"palegreen", "#98FB98"},
                                   {"paleturquoise", "#AFEEEE"},
                                   {"palevioletred", "#DB7093"},
                                   {"papayawhip", "#FFEFD5"},
                                   {"peachpuff", "#FFDAB9"},
                                   {"peru", "#CD853F"},
                                   {"pink", "#FFC0CB"},
                                   {"plum", "#DDA0DD"},
                                   {"powderblue", "#B0E0E6"},
                                   {"purple", "#800080"},
                                   {"red", "#FF0000"},
                                   {"rosybrown", "#BC8F8F"},
                                   {"royalblue", "#4169E1"},
                                   {"saddlebrown", "#8B4513"},
                                   {"salmon", "#FA8072"},
                                   {"sandybrown", "#F4A460"},
                                   {"seagreen", "#2E8B57"},
                                   {"seashell", "#FFF5EE"},
                                   {"sienna", "#A0522D"},
                                   {"silver", "#c0c0c0"},
                                   {"skyblue", "#87CEEB"},
                                   {"slateblue", "#6A5ACD"},
                                   {"slategray", "#708090"},
                                   {"snow", "#FFFAFA"},
                                   {"springgreen", "#00FF7F"},
                                   {"steelblue", "#4682B4"},
                                   {"tan", "#D2B48C"},
                                   {"teal", "#008080"},
                                   {"thistle", "#D8BFD8"},
                                   {"tomato", "#FF6347"},
                                   {"turquoise", "#40E0D0"},
                                   {"violet", "#EE82EE"},
                                   {"wheat", "#F5DEB3"},
                                   {"white", "#ffffff"},
                                   {"whitesmoke", "#F5F5F5"},
                                   {"yellow", "#FFFF00"},
                                   {"yellowgreen", "#9ACD32"},

                                   //RAL
                                   {"RAL 1000", "#cdba88"},
                                   {"RAL 1001", "#d0b084"},
                                   {"RAL 1002", "#d2aa6d"},
                                   {"RAL 1003", "#f9a800"},
                                   {"RAL 1004", "#e49e22"},
                                   {"RAL 1005", "#cb8e1e"},
                                   {"RAL 1006", "#e29000"},
                                   {"RAL 1007", "#e88c00"},
                                   {"RAL 1011", "#af804f"},
                                   {"RAL 1012", "#ddaf27"},
                                   {"RAL 1013", "#e3d9c6"},
                                   {"RAL 1014", "#ddc49a"},
                                   {"RAL 1015", "#e6d2b5"},
                                   {"RAL 1016", "#f1dd38"},
                                   {"RAL 1017", "#f6a950"},
                                   {"RAL 1018", "#faca30"},
                                   {"RAL 1019", "#a48f7a"},
                                   {"RAL 1020", "#a08f65"},
                                   {"RAL 1021", "#f6b600"},
                                   {"RAL 1023", "#f7b500"},
                                   {"RAL 1024", "#ba8f4c"},
                                   {"RAL 1026", "#ffff00"},
                                   {"RAL 1027", "#a77f0e"},
                                   {"RAL 1028", "#ff9b00"},
                                   {"RAL 1032", "#e2a300"},
                                   {"RAL 1033", "#f99a1c"},
                                   {"RAL 1034", "#eb9c52"},
                                   {"RAL 1035", "#908370"},
                                   {"RAL 1036", "#80643f"},
                                   {"RAL 1037", "#f09200"},
                                   {"RAL 2000", "#da6e00"},
                                   {"RAL 2001", "#ba481b"},
                                   {"RAL 2002", "#bf3922"},
                                   {"RAL 2003", "#f67728"},
                                   {"RAL 2004", "#e25303"},
                                   {"RAL 2005", "#ff4d06"},
                                   {"RAL 2007", "#ffb200"},
                                   {"RAL 2008", "#ed6b21"},
                                   {"RAL 2009", "#de5307"},
                                   {"RAL 2010", "#d05d28"},
                                   {"RAL 2011", "#e26e0e"},
                                   {"RAL 2012", "#d5654d"},
                                   {"RAL 2013", "#923e25"},
                                   {"RAL 3000", "#a72920"},
                                   {"RAL 3001", "#9b2423"},
                                   {"RAL 3002", "#872321"},
                                   {"RAL 3003", "#861a22"},
                                   {"RAL 3004", "#6b1c23"},
                                   {"RAL 3005", "#59191f"},
                                   {"RAL 3007", "#3e2022"},
                                   {"RAL 3009", "#6d342d"},
                                   {"RAL 3011", "#792423"},
                                   {"RAL 3012", "#c6846d"},
                                   {"RAL 3013", "#972e25"},
                                   {"RAL 3014", "#cb7375"},
                                   {"RAL 3015", "#d8a0a6"},
                                   {"RAL 3016", "#a63d2f"},
                                   {"RAL 3017", "#cb555d"},
                                   {"RAL 3018", "#c73f4a"},
                                   {"RAL 3020", "#bb1e10"},
                                   {"RAL 3022", "#cf6955"},
                                   {"RAL 3024", "#ff2d21"},
                                   {"RAL 3026", "#ff2a1b"},
                                   {"RAL 3027", "#ab273c"},
                                   {"RAL 3028", "#cc2c24"},
                                   {"RAL 3031", "#a63437"},
                                   {"RAL 3032", "#701d23"},
                                   {"RAL 3033", "#a53a2d"},
                                   {"RAL 4001", "#816183"},
                                   {"RAL 4002", "#8d3c4b"},
                                   {"RAL 4003", "#c4618c"},
                                   {"RAL 4004", "#651e38"},
                                   {"RAL 4005", "#76689a"},
                                   {"RAL 4006", "#903373"},
                                   {"RAL 4007", "#47243c"},
                                   {"RAL 4008", "#844c82"},
                                   {"RAL 4009", "#9d8692"},
                                   {"RAL 4010", "#bc4077"},
                                   {"RAL 4011", "#6e6387"},
                                   {"RAL 4012", "#6b6b7f"},
                                   {"RAL 5000", "#314f6f"},
                                   {"RAL 5001", "#0f4c64"},
                                   {"RAL 5002", "#00387b"},
                                   {"RAL 5003", "#1f3855"},
                                   {"RAL 5004", "#191e28"},
                                   {"RAL 5005", "#005387"},
                                   {"RAL 5007", "#376b8c"},
                                   {"RAL 5008", "#2b3a44"},
                                   {"RAL 5009", "#225f78"},
                                   {"RAL 5010", "#004f7c"},
                                   {"RAL 5011", "#1a2b3c"},
                                   {"RAL 5012", "#0089b6"},
                                   {"RAL 5013", "#193153"},
                                   {"RAL 5014", "#637d96"},
                                   {"RAL 5015", "#007cb0"},
                                   {"RAL 5017", "#005b8c"},
                                   {"RAL 5018", "#058b8c"},
                                   {"RAL 5019", "#005e83"},
                                   {"RAL 5020", "#00414b"},
                                   {"RAL 5021", "#007577"},
                                   {"RAL 5022", "#222d5a"},
                                   {"RAL 5023", "#42698c"},
                                   {"RAL 5024", "#6093ac"},
                                   {"RAL 5025", "#21697c"},
                                   {"RAL 5026", "#0f3052"},
                                   {"RAL 6000", "#3c7460"},
                                   {"RAL 6001", "#366735"},
                                   {"RAL 6002", "#325928"},
                                   {"RAL 6003", "#50533c"},
                                   {"RAL 6004", "#024442"},
                                   {"RAL 6005", "#114232"},
                                   {"RAL 6006", "#3c392e"},
                                   {"RAL 6007", "#2c3222"},
                                   {"RAL 6008", "#37342a"},
                                   {"RAL 6009", "#27352a"},
                                   {"RAL 6010", "#4d6f39"},
                                   {"RAL 6011", "#6c7c59"},
                                   {"RAL 6012", "#303d3a"},
                                   {"RAL 6013", "#7d765a"},
                                   {"RAL 6014", "#474135"},
                                   {"RAL 6015", "#3d3d36"},
                                   {"RAL 6016", "#00694c"},
                                   {"RAL 6017", "#587f40"},
                                   {"RAL 6018", "#61993b"},
                                   {"RAL 6019", "#b9ceac"},
                                   {"RAL 6020", "#37422f"},
                                   {"RAL 6021", "#8a9977"},
                                   {"RAL 6022", "#3a3327"},
                                   {"RAL 6024", "#008351"},
                                   {"RAL 6025", "#5e6e3b"},
                                   {"RAL 6026", "#005f4e"},
                                   {"RAL 6027", "#7ebab5"},
                                   {"RAL 6028", "#315442"},
                                   {"RAL 6029", "#006f3d"},
                                   {"RAL 6032", "#237f52"},
                                   {"RAL 6033", "#46877f"},
                                   {"RAL 6034", "#7aacac"},
                                   {"RAL 6035", "#194d25"},
                                   {"RAL 6036", "#04574b"},
                                   {"RAL 6037", "#008b29"},
                                   {"RAL 6038", "#00b51a"},
                                   {"RAL 7000", "#7a888e"},
                                   {"RAL 7001", "#8c969d"},
                                   {"RAL 7002", "#817863"},
                                   {"RAL 7003", "#7a7669"},
                                   {"RAL 7004", "#9b9b9b"},
                                   {"RAL 7005", "#6c6e6b"},
                                   {"RAL 7006", "#766a5e"},
                                   {"RAL 7008", "#745e3d"},
                                   {"RAL 7009", "#5d6058"},
                                   {"RAL 7010", "#585c56"},
                                   {"RAL 7011", "#52595d"},
                                   {"RAL 7012", "#575d5e"},
                                   {"RAL 7013", "#575044"},
                                   {"RAL 7015", "#4f5358"},
                                   {"RAL 7016", "#383e42"},
                                   {"RAL 7021", "#2f3234"},
                                   {"RAL 7022", "#4c4a44"},
                                   {"RAL 7023", "#808076"},
                                   {"RAL 7024", "#45494e"},
                                   {"RAL 7026", "#374345"},
                                   {"RAL 7030", "#928e85"},
                                   {"RAL 7031", "#5b686d"},
                                   {"RAL 7032", "#b5b0a1"},
                                   {"RAL 7033", "#7f8274"},
                                   {"RAL 7034", "#92886f"},
                                   {"RAL 7035", "#c5c7c4"},
                                   {"RAL 7036", "#979392"},
                                   {"RAL 7037", "#7a7b7a"},
                                   {"RAL 7038", "#b0b0a9"},
                                   {"RAL 7039", "#6b665e"},
                                   {"RAL 7040", "#989ea1"},
                                   {"RAL 7042", "#8e9291"},
                                   {"RAL 7043", "#4f5250"},
                                   {"RAL 7044", "#b7b3a8"},
                                   {"RAL 7045", "#8d9295"},
                                   {"RAL 7046", "#7f868a"},
                                   {"RAL 7047", "#c8c8c7"},
                                   {"RAL 7048", "#817b73"},
                                   {"RAL 8000", "#89693e"},
                                   {"RAL 8001", "#9d622b"},
                                   {"RAL 8002", "#794d3e"},
                                   {"RAL 8003", "#7e4b26"},
                                   {"RAL 8004", "#8d4931"},
                                   {"RAL 8007", "#70452a"},
                                   {"RAL 8008", "#724a25"},
                                   {"RAL 8011", "#5a3826"},
                                   {"RAL 8012", "#66332b"},
                                   {"RAL 8014", "#4a3526"},
                                   {"RAL 8015", "#5e2f26"},
                                   {"RAL 8016", "#4c2b20"},
                                   {"RAL 8017", "#442f29"},
                                   {"RAL 8019", "#3d3635"},
                                   {"RAL 8022", "#1a1718"},
                                   {"RAL 8023", "#a45729"},
                                   {"RAL 8024", "#795038"},
                                   {"RAL 8025", "#755847"},
                                   {"RAL 8028", "#513a2a"},
                                   {"RAL 8029", "#7f4031"},
                                   {"RAL 9001", "#e9e0d2"},
                                   {"RAL 9002", "#d7d5cb"},
                                   {"RAL 9003", "#ecece7"},
                                   {"RAL 9004", "#2b2b2c"},
                                   {"RAL 9005", "#0e0e10"},
                                   {"RAL 9006", "#a1a1a0"},
                                   {"RAL 9007", "#878581"},
                                   {"RAL 9010", "#f1ece1"},
                                   {"RAL 9011", "#27292b"},
                                   {"RAL 9016", "#f1f0ea"},
                                   {"RAL 9017", "#2a292a"},
                                   {"RAL 9018", "#c8cbc4"},
                                   {"RAL 9022", "#858583"},
                                   {"RAL 9023", "#797b7a"},
                               })
{
}
} // namespace signmaster