
// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <sign/Services.hpp>
#include <sign/Svg.hpp>
#include <sign/Font.hpp>

namespace signmaster
{
Svg::Svg() : xmlpp::Document()
{
    set_internal_subset("svg", "-//W3C//DTD SVG 1.1//EN", "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd");
    svgRoot = create_root_node("svg", "http://www.w3.org/2000/svg");
    svgRoot->set_namespace_declaration("http://www.w3.org/1999/xlink", "xlink");
    svgRoot->add_child_comment("signmaster generated, see http://wegemeister.de/signmaster.html by Ervin Peters 2019 ff");
    nodeDefs = svgRoot->add_child_element("defs");
};

int Svg::writeSvgToStream(std::ostream &out)
{
    write_to_stream_formatted(out);
    return 0;
}

void Svg::symbolNamePath(const std::string &symPath)
{
    _symbolNamePath = symPath;
}

std::string Svg::bitmap(const std::string &format, uint widthPX, uint heightPX)
{
    if (format == "png")
        std::cout << format << widthPX << heightPX << std::endl;
    return 0;
};

// graphic primitives
xmlpp::Element *Svg::appendLine(xmlpp::Element *item, const std::string &id, double x1, double y1, double x2, double y2, const std::string &stroke, double strokeWidth, const std::string &fill)
{
    if (!item)
        item = svgRoot;
    xmlpp::Element *line = item->add_child_element("line");
    if (id.length() > 0)
        line->set_attribute("id", id);
    line->set_attribute("x1", std::to_string(x1));
    line->set_attribute("y1", std::to_string(y1));
    line->set_attribute("x2", std::to_string(x2));
    line->set_attribute("y2", std::to_string(y2));
    if (stroke.length() > 0)
        line->set_attribute("stroke", stroke);
    line->set_attribute("stroke-width", std::to_string(strokeWidth));
    if (fill.length() > 0)
        line->set_attribute("fill", fill);
    return line;
};

xmlpp::Element *Svg::appendCircle(xmlpp::Element *item, const std::string &id, double cx, double cy, double r, const std::string &stroke, double strokeWidth, const std::string &fill)
{
    if (!item)
        item = svgRoot;
    xmlpp::Element *circle = item->add_child_element("circle");
    if (id.length() > 0)
        circle->set_attribute("id", id);
    circle->set_attribute("cx", std::to_string(cx));
    circle->set_attribute("cy", std::to_string(cy));
    circle->set_attribute("r", std::to_string(r));
    if (stroke.length() > 0)
        circle->set_attribute("stroke", stroke);
    circle->set_attribute("stroke-width", std::to_string(strokeWidth));
    if (fill.length() > 0)
        circle->set_attribute("fill", fill);
    return circle;
};

xmlpp::Element *Svg::appendPath(xmlpp::Element *item, const std::string &id, const std::string &d, const std::string &stroke, double strokeWidth, const std::string &fill)
{
    if (!item)
        item = svgRoot;
    xmlpp::Element *path = item->add_child_element("path");
    if (id.length() > 0)
        path->set_attribute("id", id);
    path->set_attribute("d", d);
    if (stroke.length() > 0)
        path->set_attribute("stroke", stroke);
    path->set_attribute("stroke-width", std::to_string(strokeWidth));
    if (fill.length() > 0)
        path->set_attribute("fill", fill);
    return path;
};

xmlpp::Element *Svg::appendRectangle(xmlpp::Element *item, const std::string &id, double x, double y, double width, double height, const std::string &stroke, double strokeWidth, const std::string &fill)
{
    if (!item)
        item = svgRoot;
    xmlpp::Element *rectangle = item->add_child_element("rect");
    if (id.length() > 0)
        rectangle->set_attribute("id", id);
    rectangle->set_attribute("x", std::to_string(x));
    rectangle->set_attribute("y", std::to_string(y));
    rectangle->set_attribute("width", std::to_string(width));
    rectangle->set_attribute("height", std::to_string(height));
    if (stroke.length() > 0)
        rectangle->set_attribute("stroke", stroke);
    rectangle->set_attribute("stroke-width", std::to_string(strokeWidth));
    if (fill.length() > 0)
        rectangle->set_attribute("fill", fill);
    return rectangle;
};

xmlpp::Element *Svg::appendSymbol(xmlpp::Element *item, const std::string &useId, double x, double y, double width, double height, const std::string &symbolName, const std::string &lib, const std::string &state)
{
    if (!item)
        item = svgRoot;
    std::string symId = _symbolNamePath + lib + '.' + symbolName;
    std::replace(symId.begin(), symId.end(), '/', '.');
    bool found = false;
    // check symbol in defs
    if (dynamic_cast<const xmlpp::ContentNode *>(nodeDefs))
    {
        for (xmlpp::Node *node : nodeDefs->get_children("symbol"))
        {
            xmlpp::Element *child = dynamic_cast<xmlpp::Element *>(node);
            if (child && (child->get_attribute("id")->get_value() == symId))
            {
                found = true;
                break;
            };
        }
    }

    if (!found)
    {
        found = srvc.sm.addSymbol(nodeDefs, _symbolNamePath, lib, symbolName, state != "Planung");
    }

    // use
    xmlpp::Element *use;
    if (found)
    {
        use = item->add_child_element("use");
        if (useId.length() > 0)
            use->set_attribute("id", useId);
        use->set_attribute("xlink:href", "#" + symId);
        use->set_attribute("x", std::to_string(x));
        use->set_attribute("y", std::to_string(y));
        use->set_attribute("width", std::to_string(width));
        use->set_attribute("height", std::to_string(height));
    }
    else
    {
        use = appendRectangle(item, useId, x, y, width, height, "red", height / 50, "grey");
    }
    return use;
};

// Text
xmlpp::Element *Svg::appendText(xmlpp::Element *item, const std::string &id, double x, double y, double &width, char alignment, const std::string &text, const std::string &fontName, double fontSize, const std::string &fontColor)
{
    if (!item)
        item = svgRoot;
    Font *font = srvc.fm.getFont(fontName);
    xmlpp::Element *textG;
    if (font)
    {
        textG = font->renderText(item, id, x, y, width, alignment, text, fontSize, fontColor);
    }
    else
    {
        std::cerr << "Font: " << fontName << " not found";
        textG = appendRectangle(item, "", x, (y - fontSize), width, fontSize, "red", 1, fontColor);
    }
    return textG;
    // ..
};

// Organisation
xmlpp::Element *Svg::appendGroup(xmlpp::Element *item, const std::string &id)
{
    if (!item)
        item = svgRoot;
    xmlpp::Element *group = item->add_child_element("g");
    if (id.length() > 0)
        group->set_attribute("id", id);
    return group;
};

void Svg::setViewBox(xmlpp::Element *item, double x, double y, double width, double height, bool setSize, const std::string &unit)
{
    if (!item)
        item = svgRoot;
    std::stringstream ss;
    ss << x << ' ' << y << ' ' << width << ' ' << height;
    if (item == nullptr)
        item = svgRoot;
    item->set_attribute("viewBox", ss.str());
    if (setSize)
    {
        item->set_attribute("x", std::to_string(x) + unit);
        item->set_attribute("y", std::to_string(y) + unit);
        item->set_attribute("width", std::to_string(width) + unit);
        item->set_attribute("height", std::to_string(height) + unit);
    }
}

} // namespace signmaster
