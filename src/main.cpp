

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <iostream>
#include <fstream>

#include <boost/program_options.hpp>

#include <sign/Services.hpp>

#include <sign/SignData.hpp>
#include <sign/Sign.hpp>

using namespace boost::program_options;
using namespace signmaster;

int main(int argc, char *argv[])
{
    // Set the global C and C++ locale to the user-configured locale,
    // so we can use std::cout with UTF-8, via Glib::ustring, without exceptions.
    //std::locale::global(std::locale(""));

    try
    {
        options_description desc{"Usage:\n    sign [options]\n"};
        desc.add_options()("help,h", "Help and usage message")("infile,i", value<std::string>()->default_value("-"), "Input File Wegemeister JSON")("outfile,o", value<std::string>()->default_value("-"), "Output File")("inputformat,d", value<std::string>()->default_value("xml"), "Input format [xml, json]")("outputformat,f", value<std::string>()->default_value("svg"), "Output format [svg, png, jpeg, bmp]")("bitmapmaxwidth,W", value<uint>()->default_value(1024), "Bitmapformats: max Image width")("bitmapmaxheight,W", value<uint>()->default_value(1024), "Bitmapformats: max Image height");

        variables_map vm;
        store(parse_command_line(argc, argv, desc), vm);
        notify(vm);

        if (vm.count("help"))
            std::cout << desc << '\n';
        else
        {
            assert(vm.count("infile"));
            assert(vm.count("outfile"));
            assert(vm.count("outputformat"));
            assert(vm.count("inputformat"));
            assert(vm.count("bitmapmaxwidth"));
            assert(vm.count("bitmapmaxheight"));

            SignData signData(vm["infile"].as<std::string>(), vm["inputformat"].as<std::string>());
            if (vm["outputformat"].as<std::string>() == "json")
                std::cout << signData.toJSON();
            else if (vm["outputformat"].as<std::string>() == "xml")
                std::cout << signData.toXML();
            else if (vm["outputformat"].as<std::string>() == "nt")
                std::cout << signData.nt();

            else
            {
                Sign *sign = signData.selectSign();
                if (sign)
                {
                    sign->createSignAsSvg("unicorn");
                    return sign->writeOutput(vm["outfile"].as<std::string>(), vm["outputformat"].as<std::string>(), vm["bitmapmaxwidth"].as<uint>(), vm["bitmapmaxheight"].as<uint>());
                }
                else
                    std::cerr << "No Sign definition found.";
            }
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
    return 0;
}
