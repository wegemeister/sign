

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <fstream>

#include <sign/Services.hpp>
#include <sign/Sign.hpp>

namespace signmaster
{

Sign::Sign(SignDataNode &node) : _node(node), _unit("mm"){

                                              };

int Sign::writeOutput(const std::string &outFileStreamName, const std::string &outFileFormat, uint bitmapMaxWidth, uint bitmapMaxHeight)
{
    bool useFile = (outFileStreamName != "-");
    std::ofstream outFileStream;

    if (useFile)
    {
        outFileStream.open(outFileStreamName);
        if (!outFileStream.is_open())
            throw SignMasterException("Could not open file for output");
    }

    std::ostream &out = useFile ? outFileStream : std::cout;
    if (outFileFormat == "svg")
        svg.writeSvgToStream(out);
    else
        svg.bitmap(outFileFormat, bitmapMaxWidth, bitmapMaxHeight);
    return 0;
};

std::string Sign::getColor(const std::string &color, const std::string &state)
{
    bool greyify = (state == "Bestand") || (state == "Abbau");
    return srvc.cm.getColor(color, greyify);
};

void Sign::createSignAsSvg(const std::string &idPrefix)
{
    throw SignMasterException("Sign: no Sign defined..." + idPrefix);
}

} // namespace signmaster