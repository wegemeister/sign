

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <iostream>
#include <exception>

#include <sign/XMLParser.hpp>

// hint https://gitlab.gnome.org/GNOME/libxmlplusplus/blob/master/examples/sax_parser/main.cc

namespace signmaster
{

XMLParser::XMLParser(SignDataNode *root) : xmlpp::SaxParser(), rootNode(root)
{
    xmlpp::Parser::set_throw_messages(true);
    currentNode = rootNode;
    rootNode->type(nodeOBJECT);
}

XMLParser::~XMLParser()
{
}

void XMLParser::on_start_document()
{
}

void XMLParser::on_end_document()
{
    if (currentNode->parent())
    {
        throw "Invalid XML structure: Parsing finished not in root.";
    }
}

void XMLParser::on_start_element(const Glib::ustring &name,
                                 const AttributeList &attributes)
{
    SignDataNode *node = new SignDataNode(name, currentNode);
    node->type(nodeOBJECT);
    // attributes to childs:
    for (const auto &attr_pair : attributes)
    {
        new SignDataNode(attr_pair.name, attr_pair.value, node);
    }
    currentNode = node;
}

void XMLParser::on_end_element(const Glib::ustring &name)
{
    if (currentNode->name() != name)
        throw "xml not wellformed";
    currentNode = currentNode->parent();
    if (!currentNode)
        throw "Invalid XML structure: tried null_ptr parent.";
}

void XMLParser::on_characters(const Glib::ustring &data)
{
    new SignDataNode("_text", data, currentNode);
}

void XMLParser::on_comment(const Glib::ustring &data)
{
    new SignDataNode("_comment", data, currentNode);
}

void XMLParser::on_warning(const Glib::ustring &text)
{
    std::cerr << "on_warning(): " << text << std::endl;
}

void XMLParser::on_error(const Glib::ustring &text)
{
    std::cerr << "on_error(): " << text << std::endl;
}

void XMLParser::on_fatal_error(const Glib::ustring &text)
{
    std::cerr << "on_fatal_error(): " << text << std::endl;
}
} // namespace signmaster