

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <fstream>

#include <sign/SignData.hpp>
#include <sign/JSONParser.hpp>
#include <sign/XMLParser.hpp>
#include <sign/Signs.hpp>

namespace signmaster
{

SignData::SignData(const std::string &data, const std::string &ftype, bool isData)
{
    if (isData)
    {
        if (ftype == "xml")
            parseXML(data);
        else if (ftype == "json")
            parseJSON(data);
    }
    else
    {
        bool useFile = (data != "-");
        std::ifstream inFileStream;

        if (useFile)
        {
            inFileStream.open(data);
            if (!inFileStream.is_open())
                throw SignMasterException("Could not open File");
        }

        std::istream &in = useFile ? inFileStream : std::cin >> std::noskipws;
        if (ftype == "xml")
            parseXML(in);
        else if (ftype == "json")
            parseJSON(in);
    }
};

void SignData::parseXML(const std::string &data)
{
    XMLParser xmlParser(&rootNode);
    xmlParser.parse_memory(data);
};

void SignData::parseXML(std::istream &inStream)
{
    XMLParser xmlParser(&rootNode);
    xmlParser.parse_stream(inStream);
};

void SignData::parseJSON(const std::string &data)
{
    JSONParser jsonParser(rootNode);
    jsonParser.parse_memory(data);
};

void SignData::parseJSON(std::istream &inStream)
{
    JSONParser jsonParser(rootNode);
    jsonParser.parse_stream(inStream);
};

std::string SignData::toJSON()
{
    return rootNode.toJSON() + "\n";
};

std::string SignData::toXML()
{
    return rootNode.toXML() + "\n";
};

std::string SignData::nt()
{
    return rootNode.nodeTypesTree() + "\n";
};

Sign *SignData::selectSign()
{
    // std::string signClass;
    if (!rootNode.child("self"))
        throw SignMasterException("Missing sign identifier");
    signClass = rootNode.child("self")->asString().substr(32);
    signClass = signClass.substr(0, signClass.find_last_of("."));

    // europe/de/de-th/Wanderwegweisertafel
    if ("europe/de/de-th/Wanderwegweisertafel" == signClass)
        return new De_Thuer_Wanderwegweiser(rootNode);
    else if ("europe/de/de-th/Wanderwegweiser" == signClass)
        return new De_Thuer_Wanderwegweiser(rootNode);
    else
        throw SignMasterException("Signclass : " + signClass + " is unknown.");
};

} // namespace signmaster
