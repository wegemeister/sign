
// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <sign/Services.hpp>
#include <sign/SymbolLibs/de_thuer_Wanderwegweisung_Wegemarken.hpp>

namespace signmaster
{
bool de_thuer_Wanderwegweisung_Wegemarken::addSymbol(xmlpp::Element *defsnode, const std::string &symbolName, bool greyify)
{
    xmlpp::Element *sym = defsnode->add_child_element("symbol");
    sym->set_attribute("id", "europe.de.de-th.Wanderwegweisung.Wegemarken." + symbolName);
    sym->set_attribute("viewBox", "0 0 100 100");
    sym->set_attribute("x", "0");
    sym->set_attribute("y", "0");
    sym->set_attribute("width", "100");
    sym->set_attribute("height", "100");

    std::string farbeWeiss = srvc.cm.getColor("#ffffff", greyify);
    std::string farbeSchwarz = srvc.cm.getColor("#000000", greyify);
    std::string farbeGruen = srvc.cm.getColor("#325928", greyify);
    std::string farbeGelb = srvc.cm.getColor("gelb", greyify);

    // Grundfläche
    if (symbolName != "Goethewanderweg")
    {
        xmlpp::Element *rahmen = sym->add_child_element("rect");
        rahmen->set_attribute("stroke", farbeSchwarz);
        rahmen->set_attribute("stroke-width", "0.5");
        rahmen->set_attribute("fill", farbeWeiss);
        rahmen->set_attribute("x", "0.25");
        rahmen->set_attribute("y", "0.25");
        rahmen->set_attribute("width", "99.5");
        rahmen->set_attribute("height", "99.5");
    }
    size_t pos = symbolName.find(' ');
    if (pos != std::string::npos)
    {
        // rote[rs], grüne[rs], gelbe[rs], blaue[rs]
        // Balken, Kreis, Quadrat, Kreuz, Dreieck
        // rote Diagonale
        std::string form = symbolName.substr(pos + 1);
        std::string farbe = srvc.cm.getColor(symbolName.substr(0, pos - ((form == "Diagonale") ? 1 : 2)), greyify);
        if (form == "Kreis" || form == "Punkt")
        {
            xmlpp::Element *kreis = sym->add_child_element("circle");
            kreis->set_attribute("stroke", "none");
            kreis->set_attribute("fill", farbe);
            kreis->set_attribute("cx", "50");
            kreis->set_attribute("cy", "50");
            kreis->set_attribute("r", "25");
            return true;
        }
        else if (form == "Balken")
        {
            xmlpp::Element *balken = sym->add_child_element("rect");
            balken->set_attribute("stroke", "none");
            balken->set_attribute("fill", farbe);
            balken->set_attribute("x", "0.5");
            balken->set_attribute("y", "33.5");
            balken->set_attribute("width", "99");
            balken->set_attribute("height", "33");
            return true;
        }
        else if (form == "Quadrat")
        {
            xmlpp::Element *square = sym->add_child_element("rect");
            square->set_attribute("stroke", "none");
            square->set_attribute("fill", farbe);
            square->set_attribute("x", "33.5");
            square->set_attribute("y", "33.5");
            square->set_attribute("width", "33");
            square->set_attribute("height", "33");
            return true;
        }
        else if (form == "Dreieck")
        {
            xmlpp::Element *dreieck = sym->add_child_element("path");
            dreieck->set_attribute("stroke", "none");
            dreieck->set_attribute("fill", farbe);
            dreieck->set_attribute("d", "M 50 20 l -35 60 70 0 z");
            return true;
        }
        else if (form == "Kreuz")
        {
            xmlpp::Element *kreuz = sym->add_child_element("path");
            kreuz->set_attribute("stroke", farbe);
            kreuz->set_attribute("stroke-width", "10");
            kreuz->set_attribute("fill", "none");
            kreuz->set_attribute("d", "M 20 20 l 60 60 m -60 0 l 60 -60");
            return true;
        }
        else if (form == "Diagonale")
        {
            xmlpp::Element *path = sym->add_child_element("path");
            path->set_attribute("stroke", "none");
            path->set_attribute("fill", farbe);
            path->set_attribute("d", "M 0.5 0.5 l 0,10 89,89 10,0 0,-10 -89,-89 z");
            return true;
        }
        else if (form == "Diagonalkreuz")
        {
            xmlpp::Element *path = sym->add_child_element("path");
            path->set_attribute("stroke", "none");
            path->set_attribute("fill", farbe);
            path->set_attribute("d", "M 2 2 l 0 20 76 76 20 0 0 -20 -76 -76 z M 98 2 l 0 20 -76 76 -20 0 0 -20 76 -76 z");
            return true;
        }
        else if (form == "Rundweg")
        {
            xmlpp::Element *path = sym->add_child_element("path");
            path->set_attribute("stroke", "none");
            path->set_attribute("fill", farbe);
            path->set_attribute("d", "M 90.868284,86.041603 H 46.020588 a 36.625618,36.62225 0 1 1 36.625618,-36.62225 h 7.474616 L 75.17159,68.104174 60.222358,49.419353 h 8.222078 A 22.423848,22.421786 0 1 0 46.020588,71.841138 h 44.847696 z");
            return true;
        }
    }
    else
    {
        if (symbolName == "Geo-Pfad")
        {
            xmlpp::Element *path = sym->add_child_element("path");
            path->set_attribute("stroke", "none");
            path->set_attribute("fill", farbeGruen);
            path->set_attribute("d", "M 12.5,12.5 l 75,0 0,75 -75,0 z M 63.778332,74.457474 c 2.575256,-0.987807 5.654137,-2.56046 6.841973,-3.494772 2.135397,-1.679742 2.157073,-1.80703 1.927599,-11.327036 l -0.232084,-9.628197 -10.32794,0 -10.327938,0 0,3.718051 0,3.718051 5.363117,0.413161 c 4.349023,0.334969 5.417407,0.698835 5.650246,1.92432 0.652832,3.435977 -0.630426,5.284604 -4.961709,7.147929 -5.798491,2.494366 -11.145975,2.01491 -15.379322,-1.379171 -7.235509,-5.800955 -7.894633,-20.757761 -1.221689,-27.722828 5.521936,-5.763649 16.42601,-4.890122 20.269326,1.623838 1.583178,2.683277 1.769303,2.759121 5.461237,2.225341 5.098783,-0.737171 6.020023,-1.541196 4.960082,-4.329114 -1.416851,-3.72657 -4.785082,-7.508525 -8.41531,-9.448963 -2.937318,-1.570093 -4.719119,-1.847647 -11.725978,-1.826619 -9.047293,0.0273 -12.331021,1.149812 -18.049839,6.171031 -4.753016,4.173179 -6.740535,9.831937 -6.674892,19.004357 0.06156,8.600793 1.433082,12.930186 5.704514,18.006509 6.122995,7.276718 19.783071,9.559704 31.138607,5.204112 z");
            return true;
        }
        if (symbolName == "Rundweg")
        {
            xmlpp::Element *path = sym->add_child_element("path");
            path->set_attribute("stroke", "none");
            path->set_attribute("fill", srvc.cm.getColor("gelb", greyify));
            path->set_attribute("d", "M 90.868284,86.041603 H 46.020588 a 36.625618,36.62225 0 1 1 36.625618,-36.62225 h 7.474616 L 75.17159,68.104174 60.222358,49.419353 h 8.222078 A 22.423848,22.421786 0 1 0 46.020588,71.841138 h 44.847696 z");
            return true;
        }
        if (symbolName == "Lutherweg")
        {
            xmlpp::Element *path = sym->add_child_element("path");
            path->set_attribute("stroke", "none");
            path->set_attribute("fill", srvc.cm.getColor("#325928", greyify));
            path->set_attribute("d", "M 86.283595,80.747738 C 88.249079,78.85833 90.138543,76.968923 92.10337,75.0035 l -4.005334,2.41916 c -2.41893,1.586716 -4.686483,2.342525 -6.575959,2.342525 -0.377758,0 -0.907219,0 -1.662742,-0.150968 -2.721011,0 -7.633888,-1.436015 -14.662964,-4.307978 -7.104762,-2.872273 -12.244369,-4.232588 -15.418821,-4.081193 -1.889469,0.0753 -3.6279,0.45308 -5.366325,1.057907 -4.837531,2.569513 -8.465428,4.686018 -10.808344,6.349021 -2.342914,-3.401759 -3.779266,-5.291134 -4.232712,-5.744282 -1.511378,-1.43598 -3.552215,-2.418811 -6.197867,-3.023287 2.192205,0 5.139935,-2.343182 8.84319,-7.028924 3.779264,-4.610666 5.668735,-8.162767 5.668735,-10.505603 V 18.545411 c 0,-3.174686 3.023413,-6.726787 9.069909,-10.5056379 -2.569966,0 -6.726667,1.5870301 -12.3954,4.7613689 -6.046499,3.401121 -9.069913,6.197384 -9.069913,8.313852 v 39.150594 c 0,3.325422 -2.040836,6.87787 -6.046498,10.883747 -0.68017,0.680141 -4.081675,3.552101 -10.1281728,8.464869 1.9651528,1.209265 4.0813458,2.720943 6.1221828,4.610317 0.982576,0.831226 2.721006,2.645284 5.366328,5.441893 0.755853,0 2.947731,-1.51168 6.802352,-4.686051 4.988568,-4.005877 8.692151,-6.046334 11.186431,-6.046334 2.191875,0 7.633561,1.88944 16.401065,5.66857 8.238377,3.552135 12.849179,5.214863 13.756396,5.063815 5.214967,-0.982865 11.034739,-4.00591 17.534688,-8.918642 M 81.522077,14.086107 C 81.370703,13.63268 79.10348,12.649814 74.719728,11.213834 70.71406,9.7778534 68.068415,9.0220138 66.9348,8.8709638 65.422766,8.7199833 61.341747,10.911769 54.690104,15.522122 47.963436,20.056777 44.637942,22.92905 44.637942,24.138352 V 55.88198 c 0,2.645247 -0.604809,4.988117 -1.738427,6.953501 -1.360671,2.41857 -3.17445,3.854516 -5.593051,4.232281 0.226724,0.07565 0.453448,0.07565 0.680167,0.07565 2.116192,0 5.668737,-2.343183 10.657304,-6.953502 5.592722,-5.290494 8.389414,-9.220707 8.389414,-11.714904 V 20.888247 c 0,-0.680141 0.378088,-0.98252 1.133941,-0.98252 0.680172,0 2.56964,0.680141 5.744093,2.040771 2.872046,1.209267 4.383756,1.738045 4.610479,1.586996 8.918876,-5.668224 13.226939,-8.842945 13.000215,-9.447422");
            path = sym->add_child_element("path");
            path->set_attribute("stroke", "none");
            path->set_attribute("fill", farbeGelb);
            path->set_attribute("d", "");
            return true;
        }
        if (symbolName == "Goethewanderweg")
        {
            xmlpp::Element *path = sym->add_child_element("rect");
            path->set_attribute("stroke", farbeWeiss);
            path->set_attribute("stroke-width", "2");
            path->set_attribute("fill", "none");
            path->set_attribute("x", "1");
            path->set_attribute("y", "2");
            path->set_attribute("width", "98");
            path->set_attribute("height", "98");

            path = sym->add_child_element("path");
            path->set_attribute("stroke", "none");
            path->set_attribute("fill", farbeWeiss);
            path->set_attribute("d", "m 66.507004,63.610227 -10.174649,0.07983 -0.03928,-3.727344 30.383093,0.03306 -0.02695,3.583056 -6.481476,-0.02825 c 0,9.572126 -2.999106,15.625518 -9.052889,19.797026 -6.053781,4.134255 -12.382731,6.201411 -18.986858,6.201411 -9.649362,0 -18.240037,-3.007705 -24.660716,-10.829292 C 21.0466,70.898138 18.39192,60.851248 18.39192,50.38523 c 0,-10.615009 3.28372,-19.796058 9.851155,-27.543147 6.567434,-7.784336 14.749208,-11.676506 24.545332,-11.676506 5.063167,0 10.295145,1.518047 14.806903,4.413032 1.394205,0.893898 1.892371,1.251694 2.697925,1.257599 0.781394,0.0057 2.466016,-1.40075 3.640082,-3.523749 l 2.33902,-0.03991 0.06442,19.613208 -3.453896,-0.06612 C 69.615354,20.652942 63.052986,15.256604 52.679401,15.030832 46.773732,14.9023 42.683612,16.841843 39.968586,19.560775 c -2.715035,2.681686 -3.931993,5.358735 -5.341524,9.77238 -1.142693,3.64547 -2.598827,10.56571 -2.862261,20.865445 -0.247064,9.659707 1.849895,18.434785 3.302533,23.036807 1.452637,4.602025 3.162973,6.882327 5.951385,9.154309 2.788405,2.234741 6.121953,2.793354 10.157806,2.793354 3.228683,0 6.229276,-0.430942 8.74253,-1.771795 2.513246,-1.34085 3.540813,-3.075361 4.654376,-5.179694 1.113562,-2.104334 1.758758,-5.493895 1.855014,-10.519037 z");
            return true;
        }
    }
    return false;
}
} // namespace signmaster