

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <iostream>
#include <string>

#include <sign/JSONParser.hpp>

namespace signmaster
{

JSONParser::JSONParser(SignDataNode &root) : rootNode(root)
{
    currentNode = &root;
}

JSONParser::~JSONParser()
{
}

void JSONParser::parse_memory(const std::string &data)
{
    // rapidjason has it's own StringStream...
    rapidjson::Reader reader;
    rapidjson::StringStream ss(data.c_str());
    if (!reader.Parse(ss, *this))
    {
        if ((reader.GetParseErrorCode() != rapidjson::kParseErrorDocumentEmpty) or (data == ""))
            throw SignMasterException("RapidJsonParseError: " + std::to_string(reader.GetParseErrorCode()));
    }
}

void JSONParser::parse_stream(std::istream &inStream)
{
    // rapidjason and can't handle std::istream...
    std::istreambuf_iterator<char> begin(inStream), end;
    std::string data(begin, end);

    parse_memory(data);
}

// -------- Parse Handler

void JSONParser::climb()
{
    if (currentNode)
        currentNode = currentNode->parent();
    else
        throw SignMasterException("JSON parse error: root level parent requested...");
    if (currentNode)
        inArray = (currentNode->type() == nodeARRAY);
}

void JSONParser::checkInArray()
{
    if (inArray)
    {
        currentNode = new SignDataNode(std::to_string(currentNode->countChildren()), currentNode);
    }
    else if (currentNode->type() != nodeEMPTY)
        throw SignMasterException("JSON parse error: overwrite exting value");
}

bool JSONParser::StartObject()
{
    checkInArray();
    currentNode->type(nodeOBJECT);
    inArray = false;
    return true;
}

bool JSONParser::EndObject(rapidjson::SizeType memberCount)
{
    if (memberCount != (uint)currentNode->countChildren())
        throw SignMasterException("JSON parse error: member count mismatch.");
    climb();
    return true;
}

bool JSONParser::Key(const char *str, rapidjson::SizeType length, bool copy)
{
    // copy indicates the need of making a copy. Since it is copied in every case
    // there is no need to look at.
    if (!str)
        throw SignMasterException("Nullpointer in stringconstruction");

    std::string name(str, length);
    currentNode = new SignDataNode(name, currentNode);
    return true;
}

bool JSONParser::StartArray()
{
    checkInArray();
    currentNode->type(nodeARRAY);
    inArray = true;
    return true;
}

bool JSONParser::EndArray(rapidjson::SizeType elementCount)
{
    if (elementCount != (uint)currentNode->countChildren())
        throw SignMasterException("JSON parse error: element count mismatch.");
    climb();
    return true;
}

bool JSONParser::Null()
{
    checkInArray();
    currentNode->type(nodeNULL);
    climb();
    return true;
}

bool JSONParser::Bool(bool b)
{
    checkInArray();
    currentNode->Bool(b);
    climb();
    return true;
}

bool JSONParser::Int(int i)
{
    checkInArray();
    currentNode->Int64(i);
    climb();
    return true;
}

bool JSONParser::Uint(unsigned u)
{
    checkInArray();
    currentNode->Int64(u);
    climb();
    return true;
}

bool JSONParser::Int64(int64_t i)
{
    checkInArray();
    currentNode->Int64(i);
    climb();
    return true;
}

bool JSONParser::Uint64(uint64_t u)
{
    checkInArray();
    if (u > INT64_MAX)
        currentNode->String(std::to_string((unsigned long)u));
    else
        currentNode->Int64(u);
    climb();
    return true;
}

bool JSONParser::Double(double d)
{
    checkInArray();
    currentNode->Double(d);
    climb();
    return true;
}

bool JSONParser::RawNumber(const char *str, rapidjson::SizeType length, bool copy)
{
    if (!str)
        throw SignMasterException("Nullpointer in stringconstruction");
    std::string value(str, length);
    checkInArray();
    currentNode->String(value);
    climb();
    return true;
}

bool JSONParser::String(const char *str, rapidjson::SizeType length, bool copy)
{
    if (!str)
        throw SignMasterException("Nullpointer in stringconstruction");
    std::string value(str, length);
    checkInArray();
    currentNode->String(value);
    climb();
    return true;
}

} // namespace signmaster