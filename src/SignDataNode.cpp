

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <numeric>

#include "sign/SignDataNode.hpp"

namespace signmaster
{
// construction and destruction
SignDataNode::SignDataNode(SignDataNode *parent)
{
    _parent = parent;
    _name = parent ? "" : "root";
    _nodeType = nodeEMPTY;
    if (parent)
        parent->addChild(this);
}

SignDataNode::SignDataNode(const std::string &name, SignDataNode *p)
{
    if (p == nullptr)
        throw SignMasterException("Parent missing in SignDataNodeCreation...");
    if (name.length() == 0)
        throw SignMasterException("Missing SignDataNodeName");
    _nodeType = nodeEMPTY;
    _name = name;
    _parent = p;
    if (p)
        p->addChild(this);
};

SignDataNode::SignDataNode(const std::string &name, const std::string &value, SignDataNode *p)
{
    if (p == nullptr)
        throw SignMasterException("Parent missing in SignDataNodeCreation...");
    if (name.length() == 0)
        throw SignMasterException("Missing SignDataNodeName");
    _nodeType = nodeSTRING;
    _name = name;
    _parent = p;
    if (p)
        p->addChild(this);
    String(value);
};

SignDataNode::~SignDataNode()
{
    for (auto child : _children)
    {
        delete child;
    }
}

void SignDataNode::type(NodeType t)
{
    _nodeType = t;
}

NodeType SignDataNode::type()
{
    return _nodeType;
}

std::string SignDataNode::name()
{
    return _name;
};

std::string SignDataNode::name(const std::string &name)
{
    _name = name;
    return _name;
};

void SignDataNode::Bool(bool val)
{
    _int64Value = val;
    _nodeType = nodeBOOL;
};

void SignDataNode::Int64(int64_t val)
{
    _int64Value = val;
    _nodeType = nodeINT64;
};

void SignDataNode::String(const std::string &val)
{
    _strValue = val;
    _nodeType = nodeSTRING;
}

void SignDataNode::Double(double d)
{
    _doubleValue = d;
    _nodeType = nodeDOUBLE;
}

//
bool SignDataNode::hasName()
{
    return (_name.length() > 0);
}

unsigned long SignDataNode::countChildren()
{
    return (_children.size());
}

// Handling children
void SignDataNode::addChild(SignDataNode *child)
{
    if (child)
        _children.push_back(child);
    else
        throw SignMasterException("addChild(nullptr) ...");
}

bool SignDataNode::hasChildren()
{
    return (_children.size() > 0);
}

SignDataNode *SignDataNode::child(uint no)
{
    if (no < _children.size())
        return _children[no];
    throw SignMasterException("child doesn't exist");
};

SignDataNode *SignDataNode::child(std::string name)
{
    if (_children.size() > 0)
    {
        std::vector<SignDataNode *>::iterator it = std::find_if(_children.begin(), _children.end(), [&name](SignDataNode *item) -> bool { return (name == item->_name); });
        if (it != _children.end())
            return (*it);
    }
    return nullptr;
};

std::string SignDataNode::childrenToJSON(unsigned long level, bool inArray)
{
    std::string json;

    for (SignDataNode *child : _children)
    {
        json += (json.length() != 0) ? ",\n" : "\n";
        json += child->toJSON(level, inArray);
    }
    return json;
}

std::string SignDataNode::toJSON(unsigned long level, bool inArray)
{
    const std::string indentString = std::string(level * 2, ' ');

    std::string json;
    if ((_name != "root" || level > 0) && !inArray)
        json += indentString + '"' + _name + "\" : ";
    else
        json += indentString;
    switch (_nodeType)
    {
    case nodeEMPTY:
        throw SignMasterException("JSON create error: unexpected empty node");
    case nodeOBJECT:
        json += "{";
        json += childrenToJSON(level + 1) + "\n" + indentString + "}";
        break;
    case nodeARRAY:
        json += "[";
        json += childrenToJSON(level + 1, true) + "\n" + indentString + "]";
        break;
    case nodeNULL:
        json += "null";
        break;
    case nodeSTRING:
        json += '"' + _strValue + '"';
        break;
    case nodeBOOL:
        json += (_int64Value) ? "true" : "false";
        break;
    case nodeINT64:
        json += std::to_string(_int64Value);
        break;
    case nodeDOUBLE:
        json += std::to_string(_doubleValue);
        break;
    default:
        break;
    }

    return json;
};

std::string SignDataNode::toXML(unsigned long level)
{
    const std::string indentString = std::string(level * 2, ' ');
    std::string xml;

    return xml;
};

std::string SignDataNode::asString()
{
    switch (_nodeType)
    {
    case nodeNULL:
        return "null";
    case nodeSTRING:
        return _strValue;
    case nodeINT64:
        return toString(_int64Value);
    case nodeBOOL:
        return (_int64Value) ? "true" : "false";
    case nodeDOUBLE:
        return toString(_doubleValue);
    case nodeEMPTY:
        throw SignMasterException("SignDataNode::asString: unexpected empty node");
    default:
        throw SignMasterException("SignDataNode::asString: unexpected node");
    }
}

int64_t SignDataNode::asInt64()
{
    if (_nodeType == nodeINT64)
        return _int64Value;
    else
        throw SignMasterException("SignDataNode::asBool(): Type mismatch");
}

double SignDataNode::asDouble()
{
    if (_nodeType == nodeDOUBLE)
        return _doubleValue;
    else if (_nodeType == nodeINT64)
        return _int64Value;
    else
        throw SignMasterException("SignDataNode::asDouble(): Type mismatch");
}

bool SignDataNode::asBool()
{
    if (_nodeType == nodeBOOL)
        return _int64Value;
    else
        throw SignMasterException("SignDataNode::asBool(): Type mismatch");
}

std::string SignDataNode::asBoolString()
{
    return _int64Value ? "true" : "false";
}

// Debugging und Testting
std::string SignDataNode::nodeTypesTree(unsigned long level)
{
    const std::string indentString = std::string((level << 1), ' ');
    std::string ret = indentString + nodeTypeNames[_nodeType] + '\n';
    for (auto child : _children)
    {
        ret += child->nodeTypesTree(level + 1);
    }

    return ret;
}

std::string SignDataNode::toString(double d)
{
    std::stringstream ss;
    ss.precision(::std::numeric_limits<double>::digits10);
    ss.unsetf(::std::ios::dec);
    //ss.setf(::std::ios::scientific);
    ss << d;
    std::string str;
    ss >> str;
    return str;
};

std::string SignDataNode::toString(int64_t d)
{
    std::stringstream ss;
    //ss.precision(::std::numeric_limits<double>::digits10);
    ss.unsetf(::std::ios::dec);
    //ss.setf(::std::ios::scientific);
    ss << d;
    std::string str;
    ss >> str;
    return str;
};

} // namespace signmaster
