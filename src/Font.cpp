

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <sign/Font.hpp>

namespace signmaster
{
// (c) 2018 Ervin Peters

Font::Font(std::string fontFileName) : xmlpp::SaxParser()
{

    xmlpp::Parser::set_throw_messages(true);
    parse_file(fontFileName);
}

void Font::on_start_document(){

};

void Font::on_end_document(){

};

void Font::on_start_element(const Glib::ustring &name,
                            const AttributeList &properties)
{

    if (name == "glyph")
    {
        gunichar chr = ' ';
        std::string d;
        double width = 0;
        for (auto p : properties)
        {
            if (p.name == "unicode")
                chr = p.value[0];
            else if (p.name == "d")
                d = p.value;
            else if (p.name == "horiz-adv-x")
                width = std::stod(p.value);
        }
        Glyph *glyph = new Glyph(chr, d, width);
        glyphs[chr] = glyph;
    }
    else if (name == "font")
    {
        //Fontgeometry see https: //www.w3.org/TR/SVG/fonts.html//FontElement
        for (auto p : properties)
        {
            if (p.name == "horiz-origin-x")
                _horizOriginX = std::stod(p.value);
            else if (p.name == "horiz-origin-y")
                _horizOriginY = std::stod(p.value);
            else if (p.name == "horiz-adv-x")
            {
                _horizAdvX = std::stod(p.value);
                if (_vertOriginX != 0.0)
                    _vertOriginX = _horizAdvX / 2;
            }
            else if (p.name == "vert-origin-x")
                _vertOriginX = std::stod(p.value);
            else if (p.name == "vert-origin-y")
                _vertOriginY = std::stod(p.value);
            else if (p.name == "vert-adv-x")
                _vertAdvX = std::stod(p.value);
        }
    }
    else if (name == "font-face")
    {
        // //https: //www.w3.org/TR/SVG/fonts.html//FontFaceElement
        for (auto p : properties)
        {
            if (p.name == "ascent-height" || p.name == "ascent")
                _ascentHeight = std::stod(p.value);
            else if (p.name == "descent-height" || p.name == "descent")
                _descentHeight = std::stod(p.value);
            // else if (p.name == "font-family");
            // $self->{'font-style'} = $f->{'font-style'};
            // $self->{'font-weight'} = $f->{'font-weight'};
            // $self->{'font-stretch'} = $f->{'font-stretch'};

            else if (p.name == "units-per-em")
                _unitsPerEm = std::stod(p.value);
            else if (p.name == "cap-height")
                _capHeight = std::stod(p.value);
            else if (p.name == "x-height")
                _xHeight = std::stod(p.value);
        }
    }
    else if (name == "hkern")
    {
        gunichar u1;
        gunichar u2;
        double hkern;
        for (auto p : properties)
        {
            if (p.name == "u1")
                u1 = p.value[0];
            else if (p.name == "u2")
                u2 = p.value[0];
            else if (p.name == "k")
                hkern = std::stod(p.value);
        }
        glyphs[u1]->hKern(u2, hkern);
    }
    _scaleFactor = _ascentHeight / (_ascentHeight - _descentHeight);
}; // namespace signmaster

void Font::on_end_element(const Glib::ustring &name){

};
void Font::on_characters(const Glib::ustring &characters){

};
void Font::on_comment(const Glib::ustring &text){

};
void Font::on_warning(const Glib::ustring &text){

};
void Font::on_error(const Glib::ustring &text){

};
void Font::on_fatal_error(const Glib::ustring &text){

};

Font::~Font()
{
    for (auto p : glyphs)
    {
        delete p.second;
    }
}

xmlpp::Element *Font::renderText(xmlpp::Element *item, const std::string &id, double x, double y, double &width, char alignment, const Glib::ustring &text, double fontSize, const std::string &fontColor)
{
    xmlpp::Element *pg = item->add_child_element("g");
    pg->set_attribute("id", id);

    if (text.empty())
        return pg;

    const double scale = fontSize / _ascentHeight;

    double xPos = 0.0;
    //Fonthöhe zur Grundlinie
    double yPos = _ascentHeight;

    Glyph *glyph;
    Glyph *oldGlyph;
    xmlpp::Element *svgChar;
    gunichar oldChr = 0;
    for (gunichar chr : text)
    {
        if (oldChr)
            xPos -= oldGlyph->hKern(chr);
        glyph = glyphs.at(chr);
        svgChar = pg->add_child_element("path");
        svgChar->set_attribute("stroke", "none");
        svgChar->set_attribute("fill", fontColor);
        svgChar->set_attribute("transform", "translate(" + std::to_string(xPos) + ',' + std::to_string(yPos) + ")");
        svgChar->set_attribute("d", glyph->pathDefinition());
        xPos += glyph->width();
        oldChr = chr;
        oldGlyph = glyph;
    }
    double dx = 0.0;
    if (alignment != 'l')
    {
        dx = (width - xPos * scale) / ((alignment == 'r') ? 1 : 2);
    }
    width = xPos * scale;
    std::string transform = "translate(" + std::to_string(x + dx) + ',' + std::to_string(y + fontSize) + ") scale(" + std::to_string(scale) + "," + std::to_string(-scale) + ") ";
    pg->set_attribute("transform", transform);
    return pg;
}

} // namespace signmaster
