
#include <cmath>
#include <vector>

#include <signs/De_Thuer_Wanderwegweiser.hpp>

namespace signmaster
{

De_Thuer_Wanderwegweiser::De_Thuer_Wanderwegweiser(SignDataNode &node) : Sign(node)
{
    _signType = "europe/de/de-th/Wanderwegweiser";
    svg.symbolNamePath("europe/de/de-th/Wanderwegweisung/");
}

void De_Thuer_Wanderwegweiser::createSignAsSvg(const std::string &idPrefix)
{
    SignDataNode *dNode = &_node;

    // Schildinhalte
    De_Thuer_Wanderwegweiser_Content data(dNode);
    // Layout und Montage
    De_Thuer_Wanderwegweiser_Layout layout(dNode);

    xmlpp::Element *sign = svg.appendGroup(svg.root(), idPrefix + ".root");

    const int directionFactor = (layout.Richtung == "links") ? -1 : 1;

    std::stringstream tmpss;

    if (layout.GrundForm == "Pfeil")
    {
        // Pfeilspitze weiss
        Point arrowHead{(directionFactor * (signWidth / 2.0)), (signHeight / 2.0)};

        // Schräge 45°
        const double dy0 = signHeight / 2.0;
        const double dx0 = -directionFactor * signHeight / 2.0;
        const double dx = -directionFactor * (signWidth - dy0);
        const double dxR = -dx;

        // Grundfläche Pfeil, weiß
        tmpss << "M " << arrowHead.x << ',' << arrowHead.y << " l " << dx0 << ',' << -dy0 << ' ' << dx << ",0 0," << signHeight << ' ' << dxR << ", 0 z";
        svg.appendPath(sign, idPrefix + ".gfWhite", tmpss.str(), getColor("schwarz", layout.Status), 0.1, getColor("RAL 9016", layout.Status));
        tmpss.str(std::string());
    }
    else
    {
        // Grundfläche Rechteck, weiß
        svg.appendRectangle(sign, idPrefix + ".gfWhite", (-signWidth / 2), 0, signWidth, signHeight, getColor("schwarz", layout.Status), 0.1, getColor("RAL 9016", layout.Status));
    }

    // Pfeilspitze x grün
    Point arrowHead{(directionFactor * (signWidth / 2 - sqrt(2.0) * whiteBorderWidth)), (signHeight / 2.0)};

    // Schräge 45°
    const double dy1 = signHeight / 2 - whiteBorderWidth;
    const double dx1 = -directionFactor * dy1;
    const double dx2 = -directionFactor * (signWidth - (1 + sqrt(2.0)) * whiteBorderWidth - dy1);
    const int h2 = signHeight - 2 * whiteBorderWidth;

    // Hintergrund grün
    tmpss << "M " << arrowHead.x << ',' << arrowHead.y << " l " << dx1 << ',' << -dy1 << ' ' << dx2 << ',' << "0 0," << h2 << ' ' << -dx2 << ",0 z";
    svg.appendPath(sign, idPrefix + ".gfGreen", tmpss.str(), "none", 0, getColor("RAL 6002", layout.Status));

    // Wegemarke[n]
    createWegeMarken(sign, idPrefix + ".wegeMarken", directionFactor, data.wegeMarken, layout.Status);

    //Zeilen Schrift, tx0 Startposition
    const double tx0 = -signWidth / 2 + ((directionFactor == -1) ? 107.5 : ((layout.MontagePosition == "mitte") ? 15 : 35));
    const double kmX0 = tx0 + ((layout.MontagePosition == "mitte") ? 297.5 : 277.5);

    const double textSpace = (layout.MontagePosition == "mitte") ? 287.5 : 267.5;
    const double max_l = textSpace;
    const std::string fontColor = getColor("RAL 9016", layout.Status);

    // Infozeile oben: Landkreis oder NNL oder Wegname
    createInfoZeile(sign, idPrefix + ".infoNahziel", tx0, 27, max_l, data.infoNahziel, layout.Status);

    // Infozeile unten
    createInfoZeile(sign, idPrefix + ".infoFernziel", tx0, 134, max_l, data.infoFernziel, layout.Status);

    // Nahziel
    createZeile(sign, idPrefix + ".nahziel", tx0, 62, max_l, data.nahziel, data.zielSymbole1, data.km1, kmX0, layout.Status);
    // Fernziel
    createZeile(sign, idPrefix + "fernziel", tx0, 110, max_l, data.fernziel, data.zielSymbole2, data.km2, kmX0, layout.Status);

    // Schild - Nr
    double textWidth = 80;
    if (!data.schildNummer.empty())
        svg.appendText(sign, idPrefix + ".schildnummer", kmX0, 134, textWidth, 'r', data.schildNummer, fontName, 6, fontColor);

    //Löcher
    if (layout.loecher)
    {
        if (layout.MontagePosition == "mitte")
        {
            double montagemitte = -directionFactor * 37.5;
            addBohrloch(sign, idPrefix + ".Bohrlich1", montagemitte + layout.lochabstand / 2, signHeight / 2, 4, layout.Status);
            addBohrloch(sign, idPrefix + ".Bohrlich2", montagemitte - layout.lochabstand / 2, signHeight / 2, 4, layout.Status);
        }
        else
        {
            addBohrloch(sign, idPrefix + ".Bohrlich1", -directionFactor * (signWidth / 2 - 15), 15, 4, layout.Status);
            addBohrloch(sign, idPrefix + ".Bohrlich2", -directionFactor * (signWidth / 2 - 15),
                        signHeight - 15, 4, layout.Status);
        }
    }
    svg.setViewBox(svg.root(), origin.x, origin.y, signWidth, signHeight, true, _unit);
}

void De_Thuer_Wanderwegweiser::createWegeMarken(xmlpp::Element *sign, std::string idPrefix, double directionFactor, std::vector<std::string> wegeMarken, std::string status)
{
    // Routensymbole
    unsigned long cntSymbols = wegeMarken.size();
    double symbolWidth = cntSymbols > 1 ? 30 : 40;
    double xP0 = (signWidth / 2 - 70);
    double sy0 = (signHeight - symbolWidth) / 2.0;

    //Zentrum der Plaketten
    double xP1 = directionFactor * (xP0 + ((symbolWidth == 40) ? 0 : 17.5)) - symbolWidth / 2;
    double xP2 = directionFactor * (xP0 - 17.5) - symbolWidth / 2;
    if (cntSymbols > 0)
        svg.appendSymbol(sign, idPrefix + ".wegemarke1", xP1, sy0, symbolWidth, symbolWidth, wegeMarken[0], "Wegemarken", status);

    if (cntSymbols > 1)
        svg.appendSymbol(sign, idPrefix + ".wegemarke2", xP2, sy0, symbolWidth, symbolWidth, wegeMarken[1], "Wegemarken", status);
}

void De_Thuer_Wanderwegweiser::createInfoZeile(xmlpp::Element *sign, std::string idPrefix, double posX, double posY, double max_l, std::string &info, std::string &Status)
{
    std::string delimiter = "$";
    std::vector<std::string> colors{getColor("RAL 9016", Status), getColor("RAL 1016", Status)};
    size_t pos = 0;
    size_t oldpos = 0;
    int cnt = 0;
    std::string token;

    int fontSize = 11;

    double textWidth = max_l;
    while ((pos = info.find(delimiter, oldpos)) != std::string::npos)
    {
        token = info.substr(oldpos, pos - oldpos);
        textWidth = max_l;
        if (!token.empty())
            svg.appendText(sign, idPrefix, posX, posY, textWidth, 'l', token, fontName, fontSize, colors[(cnt++ & 1)]);
        posX += textWidth;
        max_l -= textWidth;
        oldpos = ++pos;
    }

    if (oldpos < info.length())
        svg.appendText(sign, idPrefix, posX, posY, textWidth, 'l', info.substr(oldpos), fontName, fontSize, colors[(cnt++ & 1)]);
    if (textWidth > max_l)
        throw SignMasterException("De_Thuer_Wanderwegweiser: Info zu lang");
}

void De_Thuer_Wanderwegweiser::createZeile(xmlpp::Element *sign, std::string idPrefix, double posX, double posY, double max_l, std::string &ziel, std::vector<std::string> zielSymbole, std::string km, double posKmX, std::string &Status)
{
    xmlpp::Element *zeile0 = svg.appendGroup(sign, idPrefix);
    double symbolWidth = 20;
    int fontSize = 22;

    const std::string fontColor = getColor("RAL 9016", Status);

    unsigned long cntSymbols = zielSymbole.size();

    double textMaxWidth = max_l;

    if (cntSymbols > 0)
        textMaxWidth -= cntSymbols * symbolWidth + (cntSymbols - 1) * 3.0 + 4.5;

    double textWidth = textMaxWidth;
    if (!ziel.empty())
        svg.appendText(zeile0, idPrefix + ".ziel", posX, posY, textWidth, 'l', ziel, fontName, fontSize, fontColor);

    if (textWidth > textMaxWidth)
        throw SignMasterException("De_Thuer_Wanderwegweiser: Name zu lang");

    if (cntSymbols > 0)
    {
        double sx = posX + textMaxWidth + 4.5;
        for (std::string symbolName : zielSymbole)
        {
            svg.appendSymbol(zeile0, idPrefix + "." + symbolName, sx, posY - symbolWidth, symbolWidth, symbolWidth, symbolName, "Zielsymbole", Status);
            sx += 23;
        }
    }

    // km
    textWidth = 80;
    if (!km.empty())
        svg.appendText(zeile0, idPrefix + ".km", posKmX, posY, textWidth, 'r', km, fontName, fontSize, fontColor);
}

xmlpp::Element *De_Thuer_Wanderwegweiser::addBohrloch(xmlpp::Element *item, const std::string &id, double cx, double cy, double r, const std::string &state)
{
    std::string fcolor = getColor("#bbbbcc", state);
    std::string scolor = getColor("#000000", state);
    xmlpp::Element *loch = item->add_child_element("g");
    loch->get_attribute("id", id);
    svg.appendCircle(loch, id + ".loch", cx, cy, r, scolor, 0.5, fcolor);
    double dr = r * 0.625;
    svg.appendLine(loch, id + ".l1", cx, (cy - dr), cx, (cy + dr), scolor, 0.25, "none");
    svg.appendLine(loch, id + ".l2", (cx - dr), cy, (cx + dr), cy, scolor, 0.25, "none");
    return loch;
}

std::string De_Thuer_Wanderwegweiser_Content::formatKm(uint meter)
{
    if (meter > 9950)
    {
        uint km = (uint)(meter / 1000.0 + 0.5);
        return std::to_string(km) + " km";
    }
    else
    {
        uint km = meter / 1000;
        uint hm = (uint)(((meter - km * 1000.0) / 100.0) + 0.5);
        if (hm > 9)
        {
            ++km;
            hm = 0;
        }
        if (km > 9)
            return std::to_string(km) + " km";
        else
            return std::to_string(km) + ',' + std::to_string(hm) + " km";
    }
}

De_Thuer_Wanderwegweiser_Layout::De_Thuer_Wanderwegweiser_Layout(SignDataNode *dNode)
{
    if (dNode)
    {
        if (dNode->child("Status") != nullptr)
            Status = dNode->child("Status")->asString();
        SignDataNode *aNode = dNode->child("Ausführung");
        if (aNode)
        {
            if (aNode->child("Montageposition"))
                MontagePosition = aNode->child("Montageposition")->asString();
            if (aNode->child("Richtung"))
                Richtung = aNode->child("Richtung")->asString();
            if (aNode->child("Montageart"))
                MontageArt = (aNode->child("Montageart")->asString() == "Klemmschelle") ? 1 : 0;
            if (aNode->child("Form"))
            {
                GrundForm = aNode->child("Form")->asString();
                if (GrundForm.empty())
                    GrundForm = "Pfeil";
            }
            if (aNode->child("Löcher"))
                loecher = aNode->child("Löcher")->asBool();
            if (aNode->child("Lochabstand"))
                lochabstand = aNode->child("Lochabstand")->asDouble();
        }
    }
}

De_Thuer_Wanderwegweiser_Content::De_Thuer_Wanderwegweiser_Content(SignDataNode *dNode)
{
    if (dNode)
    {
        if (dNode->child("Schildernummer") != nullptr)
            schildNummer = dNode->child("Schildernummer")->asString();
        if (dNode->child("Wegemarken" != nullptr) && dNode->child("Wegemarken")->hasChildren())
        {
            wegeMarken.push_back(dNode->child("Wegemarken")->child(0)->asString());
            if (dNode->child("Wegemarken")->countChildren() > 1)
                wegeMarken.push_back(dNode->child("Wegemarken")->child(1)->asString());
        }
        SignDataNode *zNode = dNode->child("Nahziel");
        if (zNode)
        {
            if (zNode->child("Info"))
                infoNahziel = zNode->child("Info")->asString();
            if (zNode->child("Name"))
                nahziel = zNode->child("Name")->asString();
            if (zNode->child("Entfernung"))
                km1 = formatKm((uint)zNode->child("Entfernung")->asInt64());
            SignDataNode *zmNode = zNode->child("Zielsymbole");
            if (zmNode)
            {
                for (uint i = 0; i < (uint)zmNode->countChildren(); ++i)
                {
                    zielSymbole1.push_back(zmNode->child(i)->asString());
                }
            }
        }
        zNode = dNode->child("Fernziel");
        if (zNode)
        {
            if (zNode->child("Info"))
                infoFernziel = zNode->child("Info")->asString();
            if (zNode->child("Name"))
                fernziel = zNode->child("Name")->asString();
            if (zNode->child("Entfernung"))
                km2 = formatKm((uint)zNode->child("Entfernung")->asInt64());
            SignDataNode *zmNode = zNode->child("Zielsymbole");
            if (zmNode)
            {
                for (uint i = 0; i < (uint)zmNode->countChildren(); ++i)
                {
                    zielSymbole2.push_back(zmNode->child(i)->asString());
                }
            }
        }
    }
}

} // namespace signmaster