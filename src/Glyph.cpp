

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#include <sign/Glyph.hpp>

namespace signmaster
{
Glyph::Glyph(gunichar chr, const std::string &d, double width)
{
    _chr = chr;
    _width = width;
    _pathDefinition = d;
    if (width < 0.0)
        throw SignMasterException("Glyph width negativ.");
}

Glyph::~Glyph()
{
}

void Glyph::hKern(gunichar chr, double kern)
{
    horizontalKernings[chr] = kern;
}

double Glyph::hKern(gunichar chr)
{
    auto it = horizontalKernings.find(chr);
    return (it == horizontalKernings.end()) ? 0.0 : it->second;
}
} // namespace signmaster
