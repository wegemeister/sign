# Sign

This consoleApp Takes a JSON or XML object of a Schildermeister (<https://gitlab.com/wegemeister/schildermeister>) json schema und transfers it to a graphical representation.

## Currently available Standards

* europe/de/de-th/Wanderwegweisertafel: Signllate (Wanderwegweisertafel) according to the recommendations of "Praxisleitfaden - Touristische Wanderwegkonzeption." : <http://apps.thueringen.de/de/publikationen/pic/pubdownload1744.pdf>

## Usage and Defaults

sign [options]

options :

   --help [-h] Help and usage message

   --infile [-i] InputfileName or '-' for stdin, defaults to '-'

   --inputformat [-d] Inputformat [json, xml], defaults to inputfileextension or 'xml'

   --outfile [-o] OutputfileName or '-' for stdout, defaults to '-'

   --outputformat [-f] Outputformat [svg | png | jpg | bmp], defaults to outputfileextension or 'svg'

   --bitmapmaxwidth [-W] max width in pixel for bitmapformats

   --bitmapmaxwidth [-H] max height in pixel for bitmapformats

   --version [-v] : Shows some Version information

## Example

![wegweiser](./Examples/Beispiel01.svg)

from:

```json
{
   "self": "https://wegemeister.de/Schemata/europe/de/de-th/Wanderwegweisertafel.json",
   "version": "1",
   "Schildernummer": "Schild-Nr.: 16066_042_WW_0032_A2",
   "Wegemarken": ["blauer Balken"],
   "Ausrichtung": 12,
   "Nahziel": {
      "Info": "Gebiet $Wegename$ (40ppt)",
      "Name": "Nahziel (80ppt)",
      "Entfernung": 2000,
      "Zielsymbole": ["Aussicht"]
   },
   "Fernziel": {
      "Info": "(Ergänzung zumFernziel) (40ppt)",
      "Name": "Fernziel (80ppt)",
      "Entfernung": 10300,
      "Zielsymbole": ["Bahnhof", "Übernachtung"]
   },
   "Ausführung": {
      "Richtung": "links",
      "Montageposition": "mitte",
      "Löcher": true,
      "Lochabstand": 350
   }
}
```

More examples? See Examples Folder...

## Internal Overview

It is written in C++17 and uses cmake as build system, currently working under Linux.

### Prerequisites

* libxml++3
* librsvg
* boost
* glibmm++

### uses

* rapidjson

### concepts

The main concept is to take a json/xml description and translate it to svg.

There is a class sign which has a virtual function createSign which initiates the svg creation. From this class all specialations are derived. The Handling of fonts, symbols, colors is done by some managing/caching classes.

There is a state possible, which could be one of ["Planung", "Bestand", "Abbau"], defaults to "Planung" and provides the following:

* "Planung": Standard colorized output
* "Bestand": Greyified output
* "Abbau": Greyified and red crossed output

## License

This Software is available under GPLV3, respecting the License of external components under ./External/ which are used unmodified.
Other Licensing modells available, contact the author.

## Author

(c) 2019 Ervin Peters (<coder@wegemeister.de>, <https://wegemeister.de>)  
