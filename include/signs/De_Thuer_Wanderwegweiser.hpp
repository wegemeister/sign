#ifndef DE_THUER_WANDERWEGWEISER_HPP
#define DE_THUER_WANDERWEGWEISER_HPP

#include <sign/Sign.hpp>
#include <sign/SignDataNode.hpp>

namespace signmaster
{

struct Point
{
  double x;
  double y;
};

class De_Thuer_Wanderwegweiser_Content
{
public:
  De_Thuer_Wanderwegweiser_Content(SignDataNode *dnode);
  std::vector<std::string> wegeMarken;
  std::string infoNahziel = "Nahziel Info $Wanderweg$ 11";
  std::string nahziel = "Nahziel";
  std::string fernziel = "Fernziel";
  std::string infoFernziel = "Fernziel INfo";
  std::string schildNummer = "Schildnummer fehlt!";
  std::vector<std::string> zielSymbole1;
  std::vector<std::string> zielSymbole2;
  std::string km1;
  std::string km2;
  static std::string formatKm(uint dist);
};

class De_Thuer_Wanderwegweiser_Layout
{
public:
  De_Thuer_Wanderwegweiser_Layout(SignDataNode *dnode);
  std::string Richtung = "rechts";
  std::string GrundForm = "Pfeil";
  int MontageArt = 0;
  std::string MontagePosition = "mitte";
  bool loecher = false;
  double lochabstand = 350;

  // Layout hints
  std::string Status = "Planung";
};

class De_Thuer_Wanderwegweiser : public Sign
{
public:
  De_Thuer_Wanderwegweiser(SignDataNode &node);
  virtual ~De_Thuer_Wanderwegweiser(){};
  xmlpp::Element *addBohrloch(xmlpp::Element *item, const std::string &id, double x, double y, double r, const std::string &state);

  virtual void createSignAsSvg(const std::string &idPrefix);

  void createZeile(xmlpp::Element *sign, std::string idPrefix, double posX, double posY, double max_l, std::string &ziel, std::vector<std::string> zielsymbole, std::string km, double posKmX, std::string &Status);

  void createInfoZeile(xmlpp::Element *sign, std::string idPrefix, double posX, double posY, double max_l, std::string &info, std::string &Status);

  void createWegeMarken(xmlpp::Element *sign, std::string idPrefix, double directionFactor, std::vector<std::string> wegeMarken, std::string status);

private:
  const std::string fontName = "Arial Narrow";
  const int signWidth = 500;
  const int signHeight = 150;
  const int whiteBorderWidth = 10;
  const Point origin{-signWidth / 2.0, 0.0};
};

} // namespace signmaster

#endif
