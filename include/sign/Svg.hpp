
// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#ifndef SIGN_SVG_HPP
#define SIGN_SVG_HPP

#include <iostream>
#include <sign/External.hpp>

#include <sign/SymbolManager.hpp>
#include <sign/FontManager.hpp>

namespace signmaster
{
class Svg : xmlpp::Document
{
public:
  Svg();
  Svg(SymbolManager &sm, FontManager &fm);

  xmlpp::Element *root() { return svgRoot; };
  xmlpp::Element *defs() { return nodeDefs; };

  int writeSvgToStream(std::ostream &out);
  std::string bitmap(const std::string &format, uint widthPX, uint heightPX);

  // graphic primitives
  xmlpp::Element *appendLine(xmlpp::Element *item, const std::string &id, double x1, double y1, double x2, double y2, const std::string &stroke, double strokeWidth, const std::string &fill);

  xmlpp::Element *appendCircle(xmlpp::Element *item, const std::string &id, double cx, double cy, double r, const std::string &stroke, double strokeWidth, const std::string &fill);

  xmlpp::Element *appendPath(xmlpp::Element *item, const std::string &id, const std::string &d, const std::string &stroke, double strokeWidth, const std::string &fill);

  xmlpp::Element *appendRectangle(xmlpp::Element *item, const std::string &id, double x, double y, double width, double height, const std::string &stroke, double strokeWidth, const std::string &fill);
  // Symbol
  xmlpp::Element *appendSymbol(xmlpp::Element *item, const std::string &id, double x, double y, double width, double height, const std::string &symbolName, const std::string &lib, const std::string &state);
  // Text
  xmlpp::Element *appendText(xmlpp::Element *item, const std::string &id, double x, double y, double &width, char alignment, const std::string &text, const std::string &fontName, double fontSize, const std::string &fontColor);
  // Organisation
  xmlpp::Element *appendGroup(xmlpp::Element *item, const std::string &id);

  void setViewBox(xmlpp::Element *item, double x, double y, double width, double height, bool setSize = false, const std::string &unit = "");

  void symbolNamePath(const std::string &symPath);

private:
  xmlpp::Element *svgRoot;
  xmlpp::Element *nodeDefs;
  std::string _symbolNamePath;
};
} // namespace signmaster
#endif