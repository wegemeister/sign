

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#ifndef SIGN_JSONPARSER_HPP
#define SIGN_JSONPARSER_HPP

#include <rapidjson/reader.h>
#include <rapidjson/filereadstream.h>

#include <sign/SignMasterTypesAndExceptions.hpp>
#include <sign/SignDataNode.hpp>

namespace signmaster
{

const std::string whiteSpaces = " \t\n\r";

enum parseStep
{
  IDENTIFIER = 0,
  COLON = 1,
  VALUE = 2,
  COMMA = 3,
  ARRAYVALUE = 4,
  ARRAYCOMMA = 5,
};

class JSONParser
{
public:
  JSONParser(SignDataNode &root);
  ~JSONParser();

  void parse_memory(const std::string &data);
  void parse_stream(std::istream &inStream);

  bool Null();
  bool Bool(bool b);
  bool Int(int i);
  bool Uint(unsigned u);
  bool Int64(int64_t i);
  bool Uint64(uint64_t u);
  bool Double(double d);
  bool RawNumber(const char *str, rapidjson::SizeType length, bool copy);
  bool String(const char *str, rapidjson::SizeType length, bool copy);
  bool StartObject();
  bool Key(const char *str, rapidjson::SizeType length, bool copy);
  bool EndObject(rapidjson::SizeType memberCount);
  bool StartArray();
  bool EndArray(rapidjson::SizeType elementCount);

private:
  SignDataNode &rootNode;
  SignDataNode *currentNode = nullptr;
  std::string key;
  bool inArray = false;

  void climb();
  void checkInArray();

  std::string parseString(std::string &json, std::string::iterator &pos);
  std::string parseNumber(std::string &json, std::string::iterator &pos);
};

} // namespace signmaster

#endif // JSONPARSER_JSONPARSER_HPP