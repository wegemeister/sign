

// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#ifndef SIGN_SIGNDATANODE_HPP
#define SIGN_SIGNDATANODE_HPP

#include <iostream>
#include <map>
#include <vector>

#include <sign/SignMasterTypesAndExceptions.hpp>

namespace signmaster
{

enum NodeType
{
  nodeEMPTY,
  nodeOBJECT,
  nodeARRAY,
  nodeNULL,
  nodeSTRING,
  nodeBOOL,
  nodeINT64,
  nodeDOUBLE
};

const std::vector<std::string> nodeTypeNames = {"nodeEMPTY",
                                                "nodeOBJECT",
                                                "nodeARRAY",
                                                "nodeNULL",
                                                "nodeSTRING",
                                                "nodeBOOL",
                                                "nodeINT64",
                                                "nodeDOUBLE"};

class SignDataNode;

class SignDataNode
{
public:
  SignDataNode(SignDataNode *parent = nullptr);
  SignDataNode(const std::string &name, SignDataNode *parent);
  SignDataNode(const std::string &name, const std::string &value, SignDataNode *parent);
  ~SignDataNode();

  // Encapsulation
  std::string name();
  NodeType type();

  std::string asString();
  int64_t asInt64();
  double asDouble();
  bool asBool();
  std::string asBoolString();

  void addChild(SignDataNode *child);

  std::string name(const std::string &str);
  void type(NodeType t);
  void Bool(bool val);
  void Int64(int64_t val);
  void Double(double val);
  void String(const std::string &val);

  bool hasChildren();
  unsigned long countChildren();
  SignDataNode *child(uint no);
  SignDataNode *child(std::string name);

  SignDataNode *parent() { return _parent; };

  bool hasName();

  std::string toJSON(unsigned long level = 0, bool inArray = false);
  std::string toXML(unsigned long level = 0);

private:
  NodeType _nodeType;
  std::string _name;
  std::string _strValue;
  int64_t _int64Value;
  double _doubleValue;
  SignDataNode *_parent;
  std::vector<SignDataNode *> _children{};

  std::string childrenToJSON(unsigned long level, bool inArray = false);

  std::string toString(double d);
  std::string toString(int64_t d);

public: //Debugging and Testing
  std::string nodeTypesTree(unsigned long level = 0);
};
} // namespace signmaster
#endif