
// © Ervin Peters, 2019, coder@ervnet.de

// -- en --
// This file is part of sign. <https://wegemeister.de/soft/sign/>

// sign is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// sign is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with sign. If not, see <http://www.gnu.org/licenses/>.

// -- de --
// Diese Datei ist Teil von sign. <https://wegemeister.de/soft/sign/>

// Fubar ist Freie Software: Sie können es unter den Bedingungen
// der GNU General Public License, wie von der Free Software Foundation,
// Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
// veröffentlichten Version, weiter verteilen und/oder modifizieren.

// Fubar wird in der Hoffnung, dass es nützlich sein wird, aber
// OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
// Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
// Siehe die GNU General Public License für weitere Details.

// Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
// Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.

// ----
// Also See:
// https://gitlab.com/wegemeister/sign

#ifndef SIGN_FONT_HPP
#define SIGN_FONT_HPP

#include <sign/External.hpp>

#include "Glyph.hpp"
#include <sign/SignMasterTypesAndExceptions.hpp>

namespace signmaster
{
class Font : public xmlpp::SaxParser

{
public:
  Font(std::string fontFileLocator);
  ~Font();

  xmlpp::Element *renderText(xmlpp::Element *item, const std::string &id, double x, double y, double &width, char alignment, const Glib::ustring &text, double fontSize, const std::string &fontColor);

protected:
  //overrides:
  void on_start_document() override;
  void on_end_document() override;
  void on_start_element(const Glib::ustring &name,
                        const AttributeList &properties) override;
  void on_end_element(const Glib::ustring &name) override;
  void on_characters(const Glib::ustring &characters) override;
  void on_comment(const Glib::ustring &text) override;
  void on_warning(const Glib::ustring &text) override;
  void on_error(const Glib::ustring &text) override;
  void on_fatal_error(const Glib::ustring &text) override;

private:
  std::map<gunichar, Glyph *> glyphs;
  double _scaleFactor = 1.0;
  double _ascentHeight = 0.0;  // font-face.ascent-height || ascent
  double _capHeight = 0.0;     // font-face.cap-height
  double _descentHeight = 0.0; // font-face.descent-height
  double _unitsPerEm = 0.0;    // font-face.units-per-em
  double _xHeight = 0.0;       // font-face.x-height

  double _horizAdvX = 0.0;    // font.horiz-adv-x
  double _horizOriginX = 0.0; // font.horiz-origin-x
  double _horizOriginY = 0.0; // font.horiz-origin-y
  double _vertAdvX = 0.0;     // font.vert-adv-x
  double _vertOriginX = 0.0;  // font.vert-origin-x
  double _vertOriginY = 0.0;  // font.vert-origin-y
};
} // namespace signmaster
#endif